const { parallel, src, dest, watch } = require('gulp');
const babel = require('gulp-babel');
const less = require('gulp-less');
const cleanCSS = require('gulp-clean-css');

function buildJs() {
  return src('./js/**/*.{js,jsx}')
    .pipe(babel({
      plugins: ['transform-react-jsx'],
      presets: [['@babel/preset-env', { modules: 'amd' }]]
    }))
    .pipe(dest('build/web/js'));
}

function buildLess() {
  return src('./less/[^_]*.less')
    .pipe(less())
    .pipe(cleanCSS({ 'keepBreaks': true }))
    .pipe(dest('./build/web/css'));
}

function copyStatic() {
  return src('./web/**')
    .pipe(dest('build/web'));
}

function watchFiles() {
  watch(['./less/*.less'], buildLess);
  watch(['./js/**/*.{js,jsx}'], buildJs);
  watch(['./web/**/*'], copyStatic);
}

exports.buildJs = buildJs;
exports.less = buildLess;
exports.copyStatic = copyStatic;
exports.watch = watchFiles;
exports.default = parallel(buildJs, buildLess, copyStatic);
