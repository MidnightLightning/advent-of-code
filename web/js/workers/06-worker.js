function buildMap(orbitMap) {
  let nodes = {};
  orbitMap.forEach(instruction => {
    let [target, source] = instruction.split(')');
    if (typeof nodes[target] == 'undefined') {
      nodes[target] = { id: target, parent: false, children: [] }
    }
    if (typeof nodes[source] == 'undefined') {
      nodes[source] = { id: source, parent: nodes[target], children: [] }
    } else {
      nodes[source].parent = nodes[target];
    }
    nodes[target].children.push(nodes[source]);
  });
  Object.keys(nodes).forEach(nodeId => {
    nodes[nodeId].depth = numberOfParents(nodes, nodeId);
  });
  return nodes;
}

function numberOfParents(nodes, nodeId) {
  let n = nodes[nodeId];
  if (n.parent === false) return 0;
  return numberOfParents(nodes, n.parent.id) + 1;
}

function getParents(nodes, nodeId) {
  let n = nodes[nodeId];
  if (n.parent === false) return [];
  return getParents(nodes, n.parent.id).concat(n.parent.id);
}

function sumOrbits(orbitMap) {
  const nodes = buildMap(orbitMap);
  let sum = 0;
  Object.keys(nodes).forEach(nodeId => {
    sum += nodes[nodeId].depth;
  });
  return sum;
}

function transfersNeeded(orbitMap) {
  const nodes = buildMap(orbitMap);
  let myParents = getParents(nodes, 'YOU');
  let santaParents = getParents(nodes, 'SAN');
  for (let i = 0; i < myParents.length; i++) {
    if (myParents[i] !== santaParents[i]) {
      // Here's the diverging point
      let myTransfers = myParents.length - i;
      let sanTransfers = santaParents.length - i;
      return myTransfers + sanTransfers;
    }
  }
  return 0;
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'sum-orbits': {
      let rs = sumOrbits(e.data.orbitMap);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'transfers-needed': {
      let rs = transfersNeeded(e.data.orbitMap);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
  }
};
