
function isValid(pwd) {
  let hasDoubleDigit = false;
  for (let i = 0; i < pwd.length - 1; i++) {
    if (pwd[i] == pwd[i + 1]) hasDoubleDigit = true;
    if (parseInt(pwd[i]) > parseInt(pwd[i + 1])) return false;
  }
  return hasDoubleDigit;
}

function isValid2(pwd) {
  if (isValid(pwd) === false) return false;
  // Verify there is a set of repeated digits exactly two digits long
  for (let i = 0; i < pwd.length - 1; i++) {
    if (pwd[i] == pwd[i + 1] // Number matches one ahead
      && pwd[i] != pwd[i + 2] // But does not match the digit two ahead
      && (i == 0 || pwd[i] != pwd[i - 1]) // And does not match the number behind, if it exists
    ) {
      return true;
    }
  }
  return false;
}

function checkRange({min, max, validationFunction, messageCommand, threads}) {
  if (threads == 1) {
    return new Promise((resolve, reject) => {
      let valid = [];
      for (let i = min; i <= max; i++) {
        if (validationFunction('' + i)) {
          valid.push(i);
        }
      }
      resolve(valid);
    });
  }

  // Otherwise, split into threads
  let delta = max - min;
  let chunkSize = Math.floor(delta / threads);
  let threadPromises = [];
  for (let i = 0; i < threads; i++) {
    let threadMin = min + i * chunkSize;
    let threadMax = (i == threads) ? max : threadMin + chunkSize;
    threadPromises.push(doWork({
      cmd: messageCommand,
      min: threadMin,
      max: threadMax,
      threads: 1
    }));
  }
  return Promise.all(threadPromises).then(threadResults => {
    let foundValues = threadResults.map(rs => rs.result);
    let out = [];
    for (let i = 0; i < foundValues.length; i++) {
      out = out.concat(foundValues[i]);
    }
    return out;
  });
}

function doWork(message) {
  return new Promise((resolve, reject) => {
    let worker = new Worker('04-worker.js');
    worker.onmessage = function(e) {
      resolve(e.data);
      //worker.terminate();
    };
    worker.onerror = function(e) {
      reject(e.data);
      //worker.terminate();
    };
    worker.postMessage(message);
  });
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'is-valid': {
      let rs = isValid(e.data.pwd);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'is-valid2': {
      let rs = isValid2(e.data.pwd);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'check-range': {
      checkRange({
        min: e.data.min,
        max: e.data.max,
        validationFunction: isValid,
        messageCommand: 'check-range',
        threads: e.data.threads
      }).then(rs => {
        self.postMessage({
          id: e.data.id,
          result: rs
        });
      });
      break;
    }
    case 'check-range2': {
      checkRange({
        min: e.data.min,
        max: e.data.max,
        validationFunction: isValid2,
        messageCommand: 'check-range2',
        threads: e.data.threads
      }).then(rs => {
        self.postMessage({
          id: e.data.id,
          result: rs
        });
      });
      break;
    }
  }
};
