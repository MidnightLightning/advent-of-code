function countDigits(digits) {
  let counts = {};
  for (let i = 0; i < digits.length; i++) {
    if (typeof counts[digits[i]] == 'undefined') {
      counts[digits[i]] = 1;
    } else {
      counts[digits[i]]++;
    }
  }
  return counts;
}

function calculateChecksum(digits, width, height) {
  const layerLength = width * height;
  let layerMetas = [];
  for (let i = 0; i < digits.length; i += layerLength) {
    let layer = digits.substring(i, i + layerLength);
    if (layer == '') {
      console.error(digits.length, i, layerLength);
      return 0;
    }
    layerMetas.push(countDigits(layer));
  }

  // Find the layer with the fewest Zeroes
  let foundLayer = layerMetas.reduce((min, current) => {
    if (min === false) return current;
    if (current[0] < min[0]) return current;
    return min;
  }, false);
  if (foundLayer === false) return 0;
  return foundLayer[1] * foundLayer[2];
}

function findFirstOpaque(layers, pixelOffset) {
  for (let i = 0; i < layers.length; i++) {
    if (layers[i][pixelOffset] != 2) {
      return layers[i][pixelOffset];
    }
  }
  return false;
}

function renderImage(digits, width, height) {
  const layerLength = width * height;
  let layers = [];
  for (let i = 0; i < digits.length; i += layerLength) {
    let layer = digits.substring(i, i + layerLength);
    layers.push(layer);
  }

  // Loop through pixels, finding the first non-transparent data point
  let image = [];
  for (let y = 0; y < height; y++) {
    let row = '';
    for (let x = 0; x < width; x++) {
      let pixelOffset = y * width + x;
      row += findFirstOpaque(layers, pixelOffset);
    }
    image.push(row);
  }
  return image;
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      let rs = calculateChecksum(e.data.data, e.data.width, e.data.height);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'render-image': {
      let rs = renderImage(e.data.data, e.data.width, e.data.height);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
  }
};
