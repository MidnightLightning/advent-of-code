function countValid(batch) {
  return new Promise((resolve, reject) => {
    const requiredAttributes = [
      'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'
    ];
    let numValid = parseText(batch).reduce((numValid, passport) => {
      for (let i = 0; i < requiredAttributes.length; i++) {
        if (typeof passport[requiredAttributes[i]] == 'undefined') {
          return numValid;
        }
      }
      return numValid + 1;
    }, 0);
    resolve(numValid);
  });
}

function countValid2(batch) {
  let colorRegex = /^#[a-f0-9]{6}$/;
  let pidRegex= /^[0-9]{9}$/;
  const validEyeColors = [
    'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'
  ];
  return new Promise((resolve, reject) => {
    let valid = parseText(batch).filter(passport => {
      if (typeof passport['byr'] == 'undefined' || parseInt(passport.byr) < 1920 || parseInt(passport.byr) > 2002) {
        return false;
      } else if (typeof passport['iyr'] == 'undefined' || parseInt(passport.iyr) < 2010 || parseInt(passport.iyr) > 2020) {
        return false;
      } else if (typeof passport['eyr'] == 'undefined' || parseInt(passport.eyr) < 2020 || parseInt(passport.eyr) > 2030) {
        return false;
      }

      // Check height
      if (typeof passport['hgt'] == 'undefined') return false;
      let units = passport.hgt.substr(passport.hgt.length - 2);
      let measure = parseInt(passport.hgt.substr(0, passport.hgt.length - 2));
      if (units == 'in') {
        if (measure < 59 || measure > 76) {
          return false;
        }
      } else if (units == 'cm') {
        if (measure < 150 || measure > 193) {
          return false;
        }
      } else {
        // Unknown units
        return false;
      }

      // Check hair color
      if (typeof passport['hcl'] == 'undefined') {
        return false;
      }
      if (passport.hcl.match(colorRegex) == null) {
        return false;
      }

      if (typeof passport['ecl'] == 'undefined' || validEyeColors.indexOf(passport.ecl) < 0) {
        return false;
      }

      // Check passport ID
      if (typeof passport['pid'] == 'undefined' || passport.pid.match(pidRegex) == null) {
        return false;
      }

      return true;
    });

    valid.forEach(passport => {
      console.log(passport);
    });
    resolve(valid.length);
  });
}

function parseText(batch) {
  let passports = [];
  let currentPassport = {};
  for (let i = 0; i < batch.length; i++) {
    if (batch[i] == '') {
      // Finished with this passport, start another
      passports.push(currentPassport);
      currentPassport = {};
    } else {
      // Add fields to current passport
      let pieces = batch[i].split(' ');
      pieces.forEach(piece => {
        let [key, value] = piece.split(':');
        currentPassport[key] = value;
      });
    }
  }

  // Add last passport on, if present
  if (Object.keys(currentPassport).length > 0) {
    passports.push(currentPassport);
  }
  return passports;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      countValid(e.data.arg).then(sendResult);
      break;
    }
    case 'part-two': {
      countValid2(e.data.arg).then(sendResult);
      break;
    }
    case 'parse-text': {
      sendResult(parseText(e.data.arg));
      break;
    }
  }
};
