function doTick(map, adjacentChecker, crowdedThreshold) {
  let changed = 0;
  let updatedMap = JSON.parse(JSON.stringify(map));
  for (let y = 0; y < map.length; y++) {
    updatedMap[y] = updatedMap[y].split('');
    for (let x = 0; x < map[y].length; x++) {
      let adjacentOccupied = adjacentChecker(map, x, y);
      if (map[y][x] == 'L') {
        // Become occupied if no seats around it are occupied
        if (adjacentOccupied == 0) {
          //console.info(`Empty seat at ${x},${y} becoming occupied`);
          updatedMap[y][x] = '#';
          changed++;
        }
      } else if (map[y][x] == '#') {
        // Become unoccupied if four or more seats adjacent to it are also occupied
        if (adjacentOccupied >= crowdedThreshold) {
          //console.info(`Occupied seat at ${x},${y} vacating because ${adjacentOccupied} neighbors`);
          updatedMap[y][x] = 'L';
          changed++;
        }
      }
    }
    updatedMap[y] = updatedMap[y].join('');
  }
  return {
    changed,
    map: updatedMap
  };
}

function countAdjacentOccupied(map, x, y) {
  let count = 0;
  [
    [x - 1, y - 1],
    [x, y - 1],
    [x + 1, y - 1],
    [x + 1, y],
    [x + 1, y + 1],
    [x, y + 1],
    [x - 1, y + 1],
    [x - 1, y]
  ].forEach(coord => {
    let x = coord[0];
    let y = coord[1];
    if (y < 0 || y >= map.length) return;
    if (x < 0 || x >= map[y].length) return;
    if (map[y][x] == '#') {
      count++;
    }
  });
  return count;
}

function countLineOfSightOccupied(map, x, y) {
  let count = 0;
  [
    [-1,  0],
    [-1, -1],
    [ 0, -1],
    [ 1, -1],
    [ 1,  0],
    [ 1,  1],
    [ 0,  1],
    [-1,  1]
  ].forEach(slope => {
    // Follow this X, Y velocity until an occupied seat is hit, or edge of map is hit
    let curX = x;
    let curY = y;
    while(true) {
      curX += slope[0];
      curY += slope[1];
      if (curY < 0 || curY >= map.length) return;
      if (curX < 0 || curX >= map[y].length) return;
      if (map[curY][curX] == '#') {
        count++;
        return;
      } else if (map[curY][curX] == 'L') {
        return;
      }
    }
  });
  return count;
}

function countOccupied(map) {
  let occupied = 0;
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x] == '#') {
        occupied++;
      }
    }
  }
  return occupied;
}

function findStableCount(map) {
  let currentMap = JSON.parse(JSON.stringify(map));
  while(true) {
    let rs = doTick(currentMap, countAdjacentOccupied, 4);
    if (rs.changed == 0) {
      return countOccupied(rs.map);
    }
    currentMap = rs.map;
  }
}

function findStableCount2(map) {
  let currentMap = JSON.parse(JSON.stringify(map));
  while(true) {
    let rs = doTick(currentMap, countLineOfSightOccupied, 5);
    if (rs.changed == 0) {
      return countOccupied(rs.map);
    }
    currentMap = rs.map;
  }
}


self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findStableCount(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(findStableCount2(e.data.arg));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
