function countValidColors(batch) {
  let targetColors = [ 'shiny gold' ];
  let rules = parseText(batch);
  let validColors = {};
  let changed = 0;
  do {
    changed = 0;
    rules.forEach(rule => {
      for (let i = 0; i < targetColors.length; i++) {
        const searchColor = targetColors[i];
        if (typeof validColors[rule.outer] !== 'undefined') continue; // Already a target color
        if (rule.outer == searchColor) continue; // This is the rule about the target color

        if (typeof rule.inner[searchColor] !== 'undefined') {
          targetColors.push(rule.outer);
          validColors[rule.outer] = true;
          changed++;
        }
      }
    })
  } while (changed > 0);
  return Object.keys(validColors).length;
}

function bagsInColor(color, rules) {
  let colorsInThisBag = Object.keys(rules[color]);
  if (colorsInThisBag.length == 0) {
    // This color contains no other bags; return one for itself
    return 1;
  }

  let subTotal = colorsInThisBag.reduce((subTotal, subColor) => {
    let nestedCount = bagsInColor(subColor, rules);
    return subTotal + (nestedCount * rules[color][subColor]);
  }, 0);
  // Return the child bags, plus one for itself
  return subTotal + 1;
}

function countContainedBags(batch) {
  let ruleMap = {};
  parseText(batch).forEach(rule => {
    ruleMap[rule.outer] = rule.inner;
  });
  return bagsInColor('shiny gold', ruleMap) - 1;
}
  
function parseText(batch) {
  return batch.map(line => {
    let containPos = line.indexOf(' bags contain ');
    let outerColor = line.substr(0, containPos);
    let innerStrings = line.substr(containPos + 14);
    let out = {
      outer: outerColor,
      inner: {}
    };
    if (innerStrings != 'no other bags.') {
      innerStrings = innerStrings.substr(0, innerStrings.length - 1).split(', ');
      innerStrings.forEach(str => {
        let spacePos = str.indexOf(' ');
        let num = parseInt(str.substr(0, spacePos));
        let colorName = str.substr(spacePos + 1);
        if (num > 1) {
          colorName = colorName.substr(0, colorName.length - 5);
        } else {
          colorName = colorName.substr(0, colorName.length - 4);
        }
        out.inner[colorName] = num;
      });
    }
    console.log(out);
    return out;
  });
}


self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(countValidColors(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(countContainedBags(e.data.arg));
      break;
    }
    case 'parse-text': {
      sendResult(parseText(e.data.arg));
      break;
    }
  }
};
