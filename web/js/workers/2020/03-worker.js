function doWork(map, slope) {
  return new Promise((resolve, reject) => {
    let treeCount = 0;
    const mapWidth = map[0].length;
    const mapHeight = map.length;
    let curX = 0;
    let curY = 0;
    do {
      curX += slope[0];
      curY += slope[1];
      if (curY >= mapHeight) break;
      let mapOffset = curX;
      while (mapOffset >= mapWidth) {
        mapOffset -= mapWidth;
      }
      if (map[curY][mapOffset] == '#') {
        treeCount++;
      }
      console.log(curX, curY, treeCount);
    } while (true)
    resolve(treeCount);
  });
}

function showMap(map, slope) {
  const repeats = 5;
  return new Promise((resolve, reject) => {
    let fullMap = map.map(line => {
      let out = line;
      for (let i = 0; i < repeats; i++) {
        out += line;
      }
      return out;
    });
    resolve(fullMap);
  });
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      doWork(e.data.arg, [3, 1]).then(sendResult);
      break;
    }
    case 'part-two': {
      doWork(e.data.map, e.data.slope).then(sendResult);
    }
  }
};
