function findEncryptionWeakness({ stream, windowSize }) {
  const targetNum = findBadNum({ stream, windowSize });
  let { start, end } = findContiguousSum(stream, targetNum);
  let slice = stream.slice(start, end);
  let min = 99999999999;
  let max = 0;
  slice.forEach(record => {
    if (record > max) {
      max = record;
    }
    if (record < min) {
      min = record;
    }
  });
  return min + max;
}

function findBadNum({ stream, windowSize }) {
  let window = stream.slice(0, windowSize);
  let index = windowSize;
  while (true) {
    if (!hasSumPair(window, stream[index])) {
      return stream[index];
    }

    window.shift(); // Remove first element
    window.push(stream[index]);
    index++;
    if (index > stream.length) {
      return false;
    }
  }
}

function findContiguousSum(stream, targetNum) {
  for (let i = 0; i < stream.length - 1; i++) {
    for (let j = i + 1; j < stream.length; j++) {
      // Check the sum of all items from "i" to "j"
      let sum = stream.slice(i, j).reduce((subtotal, record) => record + subtotal, 0);
      if (sum == targetNum) {
        return { start: i, end: j };
      }
    }
  }
  return false;
}

function hasSumPair(options, targetNum) {
  for (let i = 0; i < options.length - 1; i++) {
    for (let j = i + 1; j < options.length; j++) {
      if (options[i] + options[j] == targetNum) {
        return true;
      }
    }
  }
  return false;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findBadNum(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(findEncryptionWeakness(e.data.arg));
      break;
    }
    case 'parse-text': {
      sendResult(parseText(e.data.arg));
      break;
    }
  }
};
