function step(numbers) {
  // Find the last number in the list
  const lastNum = numbers[numbers.length - 1];

  // Find the last time this number appeared in the list
  let lastOccurring = numbers.lastIndexOf(lastNum, -2);
  if (lastOccurring < 0) {
    // This is the first time this number has been said
    return 0;
  }
  return numbers.length - 1 - lastOccurring;
}

function stepThrough(series, targetStep) {
  let nums = series.split(',').map(n => parseInt(n));
  let currentTurn = nums.length + 1;
  let numMap = new Int32Array(targetStep + 5);
  numMap.fill(-1);
  for (let i = 0; i < nums.length - 1; i++) {
    numMap[nums[i]] = i + 1;
  }
  let lastNumber = nums[nums.length - 1];

  while(currentTurn <= targetStep) {
    if (currentTurn % 5000000 == 0) {
      console.log(`Processing turn ${currentTurn}...`);
    }
    let nextNumber;
    // If this is the first time this number has been said, next num is zero
    if (numMap[lastNumber] < 0) {
      nextNumber = 0;
    } else {
      nextNumber = currentTurn - 1 - numMap[lastNumber];
    }

    // Save numbers and advance
    numMap[lastNumber] = currentTurn - 1;
    lastNumber = nextNumber;
    currentTurn++;
  }
  return lastNumber;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(stepThrough(e.data.arg, 2020));
      break;
    }
    case 'part-two': {
      sendResult(stepThrough(e.data.arg, 30000000));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
