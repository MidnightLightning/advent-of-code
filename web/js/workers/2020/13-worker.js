
function findBus({ startTime, buses }) {
  let busWaits = {};
  buses.forEach(busId => {
    let multiplier = Math.ceil(startTime/busId);
    let nextBus = busId * multiplier;
    busWaits[busId] = nextBus - startTime;
  });
  let minTime = 999999999;
  let minId = false;
  buses.forEach(bus => {
    if (busWaits[bus] < minTime) {
      minTime = busWaits[bus];
      minId = bus;
    }
  });
  return minTime * minId;
}

function findSynchronized({ buses }) {
  let numbers = buses.map(bus => {
    if (bus == 'x') return bus;
    return parseInt(bus);
  });
  let maxNum = numbers.reduce((curMax, num) => {
    if (num == 'x') return curMax;
    if (num > curMax) return num;
    return curMax;
  });
  let matchesNeeded = numbers.reduce((subtotal, num) => {
    if (num == 'x') return subtotal;
    return subtotal + 1;
  }, 0);

  // Jump forward by the largest number we have, looking for places where another number falls in line
  let currentJumpSize = maxNum;
  let currentMatches = 1;
  let iterations = 0;
  let testNum = maxNum - numbers.indexOf(maxNum);
  while(true) {
    testNum += currentJumpSize;

    // See how many are in the right place
    let matches = {};
    numbers.forEach((num, index) => {
      if (num == 'x') return;
      if ((testNum + index) % num == 0) {
        matches[index] = num;
      }
    });
    let numInCorrectPlace = Object.keys(matches).length;
    if (numInCorrectPlace == matchesNeeded) {
      // Found the result!
      return testNum;
    }
    if (numInCorrectPlace > currentMatches) {
      currentMatches = numInCorrectPlace;
      // All our numbers are prime, so LCM is a multiplication of all of them.
      currentJumpSize = Object.keys(matches).reduce((subtotal, i) => {
        return subtotal * matches[i];
      }, 1);
    }
    iterations++;
    if (iterations > 10000) {
      console.error('Max iterations hit!');
      return false;
    }
  }
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findBus(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(findSynchronized(e.data.arg, e.data.workerLabel));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
