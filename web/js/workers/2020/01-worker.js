function find2020Pairs(arr) {
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i+1; j < arr.length; j++) {
      if (arr[i] + arr[j] == 2020) { 
        return [arr[i], arr[j]];
      }
    }
  }
  return false;
}

function find2020Triple(arr) {
  for (let i = 0; i < arr.length - 2; i++) {
    for (let j = i+1; j < arr.length -1; j++) {
      for (let k = j+1; k < arr.length - 1; k++) {
        if (arr[i] + arr[j] + arr[k] == 2020) { 
          return [arr[i], arr[j], arr[k]];
        }
      }
    }
  }
  return false;
}

function findProduct(arr) {
  let pair = find2020Pairs(arr);
  return pair[0] * pair[1];
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'find-pairs': {
      let rs = find2020Pairs(e.data.numbers);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'solve-puzzle': {
      let rs = findProduct(e.data.numbers);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'part-two': {
      let triple = find2020Triple(e.data.numbers);
      let product = triple[0] * triple[1] * triple[2];
      self.postMessage({
        id: e.data.id,
        result: product
      });
    }
  }
};
