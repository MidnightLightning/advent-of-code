function step(memory, acc, index) {
  let cmd = memory[index];
  let startIndex = index;
  switch (cmd.op) {
    case 'acc': {
      acc = acc + cmd.modifier;
      index++;
      break;
    }
    case 'jmp': {
      index = index + cmd.modifier;
      break;
    }
    case 'nop': {
      index++;
      break;
    }
  }
  console.log(startIndex, memory[startIndex], index, acc);
  return { acc, index };
}

function accBeforeLoop(batch) {
  let seenLines = {};
  let memory = parseText(batch);
  let acc = 0;
  let index = 0;
  while(true) {
    if (seenLines[index] === true) {
      return acc;
    } else {
      seenLines[index] = true;
    }
    let rs = step(memory, acc, index);
    acc = rs.acc;
    index = rs.index;
  }
}

function findCorrupted(batch) {
  let memory = parseText(batch);

  // Run through it normally, to see where `jmp` and `nop` commands are hit
  let acc = 0;
  let index = 0;
  let seenLines = {};
  while(true) {
    if (seenLines[index] === true) {
      // Found the loop point
      break;
    } else {
      seenLines[index] = true;
    }
    let rs = step(memory, acc, index);
    acc = rs.acc;
    index = rs.index;
  }
  let possibleTargets = Object.keys(seenLines).filter(lineNumber => {
    if (memory[lineNumber].op == 'acc') return false;
    return true;
  });

  const targetIndex = memory.length;
  for (let i = 0; i < possibleTargets.length; i++) {
    let modifiedMemory = JSON.parse(JSON.stringify(memory));
    let changeLine = possibleTargets[i];
    if (memory[changeLine].op == 'jmp') {
      // Try changing it to 'nop'
      modifiedMemory[changeLine].op = 'nop';
    } else if (memory[changeLine].op == 'nop') {
      // Try changing it to 'jmp'
      modifiedMemory[changeLine].op = 'jmp';
    } else {
      continue;
    }
    console.groupCollapsed(`Changing line ${changeLine}`);

    // Try to run to end
    let acc = 0;
    let index = 0;
    let seenLines = {};
    while(true) {
      if (index == targetIndex) {
        console.groupEnd();
        return acc;
      } else if (seenLines[index] == true) {
        // Didn't work
        console.error(`Index on line ${index}, which has been visited before`);
        console.groupEnd()
        break;
      } else {
        seenLines[index] = true;
      }
      let rs = step(modifiedMemory, acc, index);
      acc = rs.acc;
      index = rs.index;
    }
  }
  // If we reach here, didn't find any line that fixes the program...
  return false;
}
  
function parseText(batch) {
  return batch.map(line => {
    let spacePos = line.indexOf(' ');
    let op = line.substr(0, spacePos);
    let modifier = parseInt(line.substr(spacePos + 1));
    return { op, modifier };
  });
}


self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(accBeforeLoop(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(findCorrupted(e.data.arg));
      break;
    }
    case 'parse-text': {
      sendResult(parseText(e.data.arg));
      break;
    }
  }
};
