function countValid(batch) {
  return new Promise((resolve, reject) => {
    const requiredAttributes = [
      'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'
    ];
    let numValid = parseText(batch).reduce((numValid, passport) => {
      for (let i = 0; i < requiredAttributes.length; i++) {
        if (typeof passport[requiredAttributes[i]] == 'undefined') {
          return numValid;
        }
      }
      return numValid + 1;
    }, 0);
    resolve(numValid);
  });
}

function sumAnswers(batch) {
  return new Promise((resolve, reject) => {
    let sum = parseText(batch).map(group => {
      let questionsAnswered = {};
      group.forEach(person => {
        for (let i = 0; i < person.length; i++) {
          questionsAnswered[person[i]] = true;
        }
      });
      return Object.keys(questionsAnswered).length;
    }).reduce((subTotal, count) => subTotal + count, 0);
    resolve(sum);
  });
}

function sumAnswers2(batch) {
  let questions = 'abcdefghijklmnopqrstuvwxyz'.split('');
  return new Promise((resolve, reject) => {
    let peopleGroups = parseText(batch);
    let sum = peopleGroups.map(group => {
      let questionsAnswered = {};
      questions.forEach(letter => {
        for (let i = 0; i < group.length; i++) {
          if (group[i].indexOf(letter) < 0) {
            return;
          }
        }
        questionsAnswered[letter] = true;
      });
      return Object.keys(questionsAnswered).length;
    }).reduce((subTotal, count) => subTotal + count, 0);
    resolve(sum);
  });
}

function parseText(batch) {
  let peopleGroups = [];
  let currentGroup = [];
  for (let i = 0; i < batch.length; i++) {
    if (batch[i] == '') {
      // Finished with this group, start another
      peopleGroups.push(currentGroup);
      currentGroup = [];
    } else {
      // Add person to current group
      currentGroup.push(batch[i]);
    }
  }

  // Add last group on, if present
  if (currentGroup.length > 0) {
    peopleGroups.push(currentGroup);
  }
  return peopleGroups;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sumAnswers(e.data.arg).then(sendResult);
      break;
    }
    case 'part-two': {
      sumAnswers2(e.data.arg).then(sendResult);
      break;
    }
    case 'parse-text': {
      sendResult(parseText(e.data.arg));
      break;
    }
  }
};
