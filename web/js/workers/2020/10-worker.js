function countDeltas(arr) {
  let deltas = { 1: 1, 3: 1 };
  let sorted = arr.sort((a, b) => {
    return a - b;
  });
  for (let i = 0; i < sorted.length - 1; i++) {
    let delta = sorted[i+1] - sorted[i];
    deltas[delta]++;
  }
  return deltas;
}

function doCalculation(arr) {
  let deltas = countDeltas(arr);
  return deltas[1] * deltas[3];
}

function getAllDeltas(arr) {
  let deltas = [];
  let sorted = arr.sort((a, b) => {
    return a - b;
  });
  sorted.unshift(0);
  for (let i = 0; i < sorted.length - 1; i++) {
    deltas.push(sorted[i + 1] - sorted[i]);
  }
  return deltas;
}

function findRunEnd(arr, start) {
  for (let j = start + 1; j < arr.length; j++) {
    if (arr[j] != '1') {
      return j;
    }
  }
  return arr.length;
}

function countOptions(arr) {
  let options = 1;
  let index = 0;
  let deltas = getAllDeltas(arr);
  while(true) {
    //debugger;
    if (index >= deltas.length) {
      return options;
    }
    if (deltas[index] == '1') {
      // See how long the run is
      let end = findRunEnd(deltas, index);
      let runSize = end - index;
      console.log(`Found run of ${runSize} at ${index}`);
      if (runSize == 2) {
        options = options * 2;
      } else if (runSize == 3) {
        options = options * 4;
      } else if (runSize == 4) {
        options = options * 7;
      }
      index = end;
    } else {
      index++;
    }
  }
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(doCalculation(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(countOptions(e.data.arg));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
    case 'deltas': {
      sendResult(getAllDeltas(e.data.arg));
    }
  }
};
