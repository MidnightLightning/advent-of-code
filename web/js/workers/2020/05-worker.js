function parseSeat(seatString) {
  let rowString = seatString.substr(0, 7);
  let colString = seatString.substr(7);
  rowString = rowString.replaceAll('F', '0').replaceAll('B', '1');
  colString = colString.replaceAll('L', '0').replaceAll('R', '1');
  let row = parseInt(rowString, 2);
  let col = parseInt(colString, 2);
  let id = row * 8 + col;
  return {
    row: row,
    column: col,
    id: id
  };
}

function findMaxID(seats) {
  let sorted = seats.map(parseSeat).sort((a, b) => {
    return b.id - a.id;
  });
  return sorted[0].id;
}

function findHole(seats) {
  let sorted = seats.map(parseSeat).sort((a, b) => {
    return b.id - a.id;
  });
  //console.log(sorted);
  for (let i = 1; i < sorted.length - 1; i++) {
    if (sorted[i].id - sorted[i + 1].id != 1) {
      return sorted[i].id - 1;
    }
  }
  return 0;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findMaxID(e.data.arg));
      break;
    }
    case 'parse-seat': {
      sendResult(parseSeat(e.data.arg));
      break;
    }
    case 'find-hole': {
      sendResult(findHole(e.data.arg));
      break;
    }
  }
};
