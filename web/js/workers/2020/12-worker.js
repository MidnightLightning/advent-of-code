
const directions = [
  'E',
  'S',
  'W',
  'N'
];

function step({ eastPos, northPos, facing, instruction }) {
  let action = instruction[0];
  let value = parseInt(instruction.substr(1));
  let curE = eastPos;
  let curN = northPos;
  let curFacing = directions.indexOf(facing);
  switch (action) {
    case 'N':
      curN += value;
      break;
    case 'S':
      curN -= value;
      break;
    case 'E':
      curE += value;
      break;
    case 'W':
      curE -= value;
      break;
    case 'L': {
      curFacing -= value/90;
      while (curFacing < 0) {
        curFacing += 4;
      }
      break;
    }
    case 'R': {
      curFacing += value/90;
      while (curFacing >= 4) {
        curFacing -= 4;
      }
      break;
    }
    case 'F':
      switch (directions[curFacing]) {
        case 'N':
          curN += value;
          break;
        case 'S':
          curN -= value;
          break;
        case 'E':
          curE += value;
          break;
        case 'W':
          curE -= value;
          break;
      }
      break;
    default:
      console.log('Unknkown Action!', action);
  }
  return {
    eastPos: curE,
    northPos: curN,
    facing: directions[curFacing]
  };
}

function findEnd(instructions) {
  let curE = 0;
  let curN = 0;
  let curFacing = 'E';
  instructions.forEach(instruction => {
    let rs = step({
      eastPos: curE,
      northPos: curN,
      facing: curFacing,
      instruction
    });
    curE = rs.eastPos;
    curN = rs.northPos;
    curFacing = rs.facing;
  });
  return Math.abs(curE) + Math.abs(curN);
}

function rotateCW(n, e) {
  return {
    n: -1 * e,
    e: n
  };
};

function step2({ eastPos, northPos, eastWaypoint, northWaypoint, facing, instruction }) {
  let action = instruction[0];
  let value = parseInt(instruction.substr(1));
  let curE = eastPos;
  let curN = northPos;
  let wpE = eastWaypoint;
  let wpN = northWaypoint;
  let curFacing = directions.indexOf(facing);
  switch (action) {
    case 'N':
      wpN += value;
      break;
    case 'S':
      wpN -= value;
      break;
    case 'E':
      wpE += value;
      break;
    case 'W':
      wpE -= value;
      break;
    case 'L': {
      let steps = value/90 * -1;
      while (steps < 0) {
        steps += 4;
      }
      for (let i = 0; i < steps; i++) {
        let rs = rotateCW(wpN, wpE);
        wpN = rs.n;
        wpE = rs.e;
      }
      break;
    }
    case 'R': {
      let steps = value/90;
      while (steps >= 4) {
        steps += 4;
      }
      for (let i = 0; i < steps; i++) {
        let rs = rotateCW(wpN, wpE);
        wpN = rs.n;
        wpE = rs.e;
      }
      break;
    }
    case 'F':
      curN += value * wpN;
      curE += value * wpE;
      break;
    default:
      console.log('Unknkown Action!', action);
  }
  return {
    eastPos: curE,
    northPos: curN,
    eastWaypoint: wpE,
    northWaypoint: wpN,
    facing: directions[curFacing]
  };
}

function findEnd2(instructions) {
  let curE = 0;
  let curN = 0;
  let wpE = 10;
  let wpN = 1;
  let curFacing = 'E';
  instructions.forEach(instruction => {
    let rs = step2({
      eastPos: curE,
      northPos: curN,
      eastWaypoint: wpE,
      northWaypoint: wpN,
      facing: curFacing,
      instruction
    });
    curE = rs.eastPos;
    curN = rs.northPos;
    wpE = rs.eastWaypoint;
    wpN = rs.northWaypoint;
    curFacing = rs.facing;
  });
  return Math.abs(curE) + Math.abs(curN);
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findEnd(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(findEnd2(e.data.arg));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
