function passwordIsValid(passwordLine) {
  return new Promise((resolve, reject) => {
    let [ rule, pass ] = passwordLine.split(': ');
    let [ count, char ] = rule.split(' ');
    let [ min, max ] = count.split('-');
    let charCount = 0;
    for (let i = 0; i < pass.length; i++) {
      if (pass[i] == char) { 
        charCount++;
      }
    }
    resolve(charCount >= min && charCount <= max);
  });
}

function password2IsValid(passwordLine) {
  return new Promise((resolve, reject) => {
    let [ rule, pass ] = passwordLine.split(': ');
    let [ count, char ] = rule.split(' ');
    let [ pos1, pos2 ] = count.split('-');
    pos1 = parseInt(pos1);
    pos2 = parseInt(pos2);
    let rs1 = pass[pos1 - 1] == char;
    let rs2 = pass[pos2 - 1] == char;

    resolve((rs1 || rs2) && !(rs1 && rs2));
  });
}

function countValid(passwords, testFunc) {
  let validPromises = passwords.map(testFunc);
  return Promise.all(validPromises).then(rs => {
    return rs.reduce((subTotal, rs) => {
      if (rs) {
        subTotal++;
      }
      return subTotal;
    }, 0);
  });
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      countValid(e.data.arg, passwordIsValid).then(rs => {
        self.postMessage({
          id: e.data.id,
          result: rs
        });
      });
      break;
    }
    case 'part-two': {
      countValid(e.data.arg, password2IsValid).then(rs => {
        self.postMessage({
          id: e.data.id,
          result: rs
        });
      });
    }
  }
};
