function parseCmd(program) {
  let equalsPos = program.indexOf(' = ');
  let cmd = program.substr(0, equalsPos);
  let value = program.substr(equalsPos + 3);
  return { cmd, value };
}

function step({ memory, mask, program }) {
  let { cmd, value} = parseCmd(program);
  if (cmd == 'mask') {
    // Update the mask value
    mask = value;
  } else {
    // Update memory sequence
    let memoryAddress = cmd.substr(4, cmd.length - 5);
    let parsedValue = parseInt(value).toString(2);
    parsedValue = ('000000000000000000000000000000000000' + parsedValue).substr(-36).split('');
    // Apply mask
    for (let i = 0; i < mask.length; i++) {
      if (mask[i] == '1') {
        parsedValue[i] = '1';
      } else if (mask[i] == '0') {
        parsedValue[i] = '0';
      }
    }
    memory[memoryAddress] = parseInt(parsedValue.join(''), 2);
  }
  return {
    memory, mask
  }
}

function setFloatingMemory(memory, address, value) {
  let bitLength = address
    .filter(bit => bit == 'X')
    .length;

  let maxNum = parseInt(address
    .filter(bit => bit == 'X')
    .map(bit => 1)
    .join(''), 2);

  for (let i = 0; i <= maxNum; i++) {
    let bits = ('00000000000000000000' + i.toString(2)).substr(-1 * bitLength).split('');
    let curAddress = JSON.parse(JSON.stringify(address));
    for (let j = 0; j < curAddress.length; j++) {
      if (curAddress[j] == 'X') {
        let nextVal = bits.shift();
        curAddress[j] = nextVal;
      }
    }
    let parsedAddress = parseInt(curAddress.join(''), 2);
    memory[parsedAddress] = value;
  }

  return memory;
}

function step2({ memory, mask, program }) {
  let { cmd, value} = parseCmd(program);
  if (cmd == 'mask') {
    // Update the mask value
    mask = value;
  } else {
    // Update memory sequence
    let memoryAddress = cmd.substr(4, cmd.length - 5);
    let parsedAddress = ('000000000000000000000000000000000000' + parseInt(memoryAddress).toString(2)).substr(-36).split('');
    let parsedValue = parseInt(value);
    // Apply mask
    for (let i = 0; i < mask.length; i++) {
      if (mask[i] == '1') {
        parsedAddress[i] = '1';
      } else if (mask[i] == 'X') {
        parsedAddress[i] = 'X';
      }
    }
    memory = setFloatingMemory(memory, parsedAddress, parsedValue);
  }
  return {
    memory, mask
  }
}

function calculateSum(program, stepFunc) {
  let memory = [];
  let mask = '';
  for (let i = 0; i < program.length; i++) {
    let rs = stepFunc({ memory, mask, program: program[i] });
    memory = rs.memory;
    mask = rs.mask
  }
  return Object.keys(memory).reduce((subtotal, key) => {
    return memory[key] + subtotal;
  }, 0);
}


self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(calculateSum(e.data.arg, step));
      break;
    }
    case 'part-two': {
      sendResult(calculateSum(e.data.arg, step2));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
