function findInvalid(ticketData) {
  let badNums = [];
  ticketData.otherTickets.forEach(ticket => {
    ticket.forEach(num => {
      // Check each rule
      let isValid = false;
      ticketData.rules.forEach(rule => {
        rule.value.forEach(range => {
          if (num >= range.min && num <= range.max) {
            isValid = true;
          }
        });
      });
      if (!isValid) {
        badNums.push(num);
      }
    });
  });
  return badNums.reduce((subTotal, num) => subTotal + num, 0);
}

function parseTicketProps(ticketData) {
  // Filter out bad tickets
  let validTickets = ticketData.otherTickets.filter(ticket => {
    for (let i = 0; i < ticket.length; i++) {
      let num = ticket[i];
      // Check each rule
      let isValid = false;
      ticketData.rules.forEach(rule => {
        rule.value.forEach(range => {
          if (num >= range.min && num <= range.max) {
            isValid = true;
          }
        });
      });
      if (!isValid) {
        return false;
      }
    }
    return true;
  });

  // Step through each rule and find the ticket offset that matches it
  let propIndexes = {};
  ticketData.rules.forEach(rule => {
    // Loop through all tickets trying each index
    let possibleCorrectIndexes = [];
    for (let i = 0; i < validTickets[0].length; i++) {
      let numInvalid = 0;
      validTickets.forEach(ticket => {
        let isValid = false;
        rule.value.forEach(range => {
          if (ticket[i] >= range.min && ticket[i] <= range.max) {
            isValid = true;
          }
        });
        if (!isValid) {
          numInvalid++;
        }
      });
      if (numInvalid == 0) {
        possibleCorrectIndexes.push(i);
      }
    }
    propIndexes[rule.propName] = possibleCorrectIndexes;
  });

  // Now step through each property name, and find any where there's only one property that can be it.
  let finalProps = {};

  do {
    let singleOption = Object.keys(propIndexes).filter(propName => {
      return (propIndexes[propName].length == 1);
    });
    if (singleOption.length == 1) {
      let propName = singleOption[0];
      let ticketIndex =  propIndexes[propName][0];
      finalProps[propName] = ticketIndex;
      delete propIndexes[propName];
      // Remove this option from all the others
      Object.keys(propIndexes).forEach(propName => {
        propIndexes[propName] = propIndexes[propName].filter(num => num !== ticketIndex);
      });
    } else {
      console.error('Fault!');
      break;
    }
  } while (Object.keys(propIndexes).length > 0);

  // FINALLY, parse our ticket into these props
  let final = {};
  Object.keys(finalProps).forEach(propName => {
    const ticketIndex = finalProps[propName];
    final[propName] = ticketData.myTicket[ticketIndex];
  });
  return final;
}

self.onmessage = function(e) {
  function sendResult(rs) {
    self.postMessage({
      id: e.data.id,
      result: rs
    });
  }
  switch (e.data.cmd) {
    case 'solve-puzzle': {
      sendResult(findInvalid(e.data.arg));
      break;
    }
    case 'part-two': {
      sendResult(parseTicketProps(e.data.arg));
      break;
    }
    case 'parse-list': {
      sendResult(countDeltas(e.data.arg));
      break;
    }
  }
};
