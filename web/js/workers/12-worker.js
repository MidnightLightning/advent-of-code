
function tick(moons) {
  let gravities = moons.map((moon, moonIndex) => {
    let gravity = [0, 0, 0];
    for (let i = 0; i < moons.length; i++) {
      if (i !== moonIndex) {
        // Compare to other moon
        if (moons[i].pos[0] < moon.pos[0]) {
          gravity[0]--;
        } else if (moons[i].pos[0] > moon.pos[0]) {
          gravity[0]++;
        }
        if (moons[i].pos[1] < moon.pos[1]) {
          gravity[1]--;
        } else if (moons[i].pos[1] > moon.pos[1]) {
          gravity[1]++;
        }
        if (moons[i].pos[2] < moon.pos[2]) {
          gravity[2]--;
        } else if (moons[i].pos[2] > moon.pos[2]) {
          gravity[2]++;
        }
      }
    }
    return gravity;
  });

  return moons.map((moon, index) => {
    const gravity = gravities[index];
    let newMoon = {
      vel: [
        moon.vel[0] + gravity[0],
        moon.vel[1] + gravity[1],
        moon.vel[2] + gravity[2]
      ]
    };
    newMoon.pos = [
      moon.pos[0] + newMoon.vel[0],
      moon.pos[1] + newMoon.vel[1],
      moon.pos[2] + newMoon.vel[2]
    ];
    return newMoon;
  });
}

function hashSystemState(moons) {
  return moons.map(moon => {
    return moon.pos.join(',') + ':' + moon.vel.join(',');
  }).join('-');
}
function findCollision(moonInitialPositions) {
  let moons = [];
  moonInitialPositions.forEach(moonPos => {
    moons.push({
      pos: [moonPos[0], moonPos[1], moonPos[2]],
      vel: [0, 0, 0]
    });
  });
  let seenStates = {};
  let lastVelocityStates = moons.map(moon => {
    return [-1, -1, -1];
  });
  let orbitPeriods = moons.map(moon => {
    return [-1, -1, -1];
  });
  for (let i = 0; i < 99999; i++) {
    moons = tick(moons);
    let stateID = hashSystemState(moons);
    if (seenStates[stateID] === true) {
      console.log(orbitPeriods);
      return i;
    }
    seenStates[stateID] = true;
    function analyzeVel(velocity, velIndex, moonIndex, axisLabel, tick, debug) {
      if (velocity !== 0) return;
      let lastSeen = lastVelocityStates[moonIndex][velIndex];
      if (lastSeen < 0) {
        if (debug) console.log(`Moon ${moonIndex} ${axisLabel} velocity to zero, tick ${tick}`);
      } else {
        if (debug) console.log(`Moon ${moonIndex} ${axisLabel} velocity to zero, tick ${tick}. Last seen in this state ${lastSeen} (${tick - lastSeen} ago)`);
        orbitPeriods[moonIndex][velIndex] = tick - lastSeen;
      }
      lastVelocityStates[moonIndex][velIndex] = tick;
    }
    if (i > 10) {
      moons.forEach((moon, index) => {
        analyzeVel(moon.vel[0], 0, index, 'X', i);
        analyzeVel(moon.vel[1], 1, index, 'Y', i);
        analyzeVel(moon.vel[2], 2, index, 'Z', i, true);
      });
    }
    if ((i+1) % 1000000 == 0) console.log(i+1, moons);
  }
  console.log(orbitPeriods);
  return false;
}


self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'find-collision': {
      let rs = findCollision(e.data.moons);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
  }
};
