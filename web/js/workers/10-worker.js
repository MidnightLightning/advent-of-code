function reduce(x, y) {
  let bigger;
  if (Math.abs(x) > Math.abs(y)) {
    bigger = Math.abs(x);
  } else {
    bigger = Math.abs(y);
  }
  for (let i = bigger; i > 0; i--) {
    if (parseInt(x / i) == x / i && parseInt(y / i) == y / i) {
      return [x / i, y / i];
    }
  }
  return [x, y];
}

function calcDistance(fromX, fromY, toX, toY) {
  let deltaX = fromX - toX;
  let deltaY = fromY - toY;
  let dist = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
  if (isNaN(dist)) {
    debugger;
  }
  return dist;
}

function isVisibleFrom(map, fromX, fromY, targetX, targetY, debug = false) {
  let deltaX = targetX - fromX;
  let deltaY = targetY - fromY;
  if (deltaX == 0 && deltaY == 0) return false;
  const width = map[0].length;
  const height = map.length;
  if (debug) console.log(map.join("\n"));
  let [vX, vY] = reduce(deltaX, deltaY);
  if (debug) console.log('Reducing vector:', deltaX, deltaY, vX, vY);
  let curX = fromX;
  let curY = fromY;
  if (debug) console.log(`Starting point: (${curX}, ${curY}), heading to (${targetX}, ${targetY}), by moving (${vX}, ${vY})`);
  while (curX >= 0 && curX <= width && curY >= 0 && curY <= height) {
    curX += vX;
    curY += vY;
    if (curX == targetX && curY == targetY) {
      if (debug) console.log('Landed on target without hitting anything else; visible!');
      return true;
    }
    if (map[curY][curX] == '#') {
      if (debug) console.log(`Found asteroid at (${curX}, ${curY}), on our way from (${fromX}, ${fromY}) to (${targetX}, ${targetY}); not visible.`);
      return false;
    }
  }
  if (debug) console.log(`Going from (${fromX}, ${fromY}) to (${targetX}, ${targetY}), by moving (${vX}, ${vY}), hit the edge of the map without hitting anything...?`);
  return true;
}

function findVisible(map, targetX, targetY, debug = false) {
  const width = map[0].length;
  const height = map.length;
  let seen = [];
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      if (map[y][x] == '#' && isVisibleFrom(map, targetX, targetY, x, y, debug)) {
        seen.push([x, y]);
      }
    }
  }
  return seen;
}


function findBest(map, debug = false) {
  const width = map[0].length;
  const height = map.length;
  let maxCoord = false;
  let maxSeen = 0;
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      if (map[y][x] == '#') {
        // Asteroid here
        let seen = findVisible(map, x, y, debug);
        if (debug) console.log(`From (${x}, ${y}), seen:`, seen);
        if (seen.length > maxSeen) {
          maxCoord = [x, y];
          maxSeen = seen.length;
        }
      }
    }
  }
  return {
    coordinate: maxCoord,
    visible: maxSeen
  };
}

function vaporizedOrder(map, stationX, stationY, debug = false) {
  // For each asteroid, find the vector and multiple it is from the station
  const width = map[0].length;
  const height = map.length;
  let asteroids = {};
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      if (map[y][x] == '#' && !(x == stationX && y == stationY)) {
        // Asteroid here
        let deltaX = x - stationX;
        let deltaY = y - stationY;
        let [vX, vY] = reduce(deltaX, deltaY);
        let id = vX + ':' + vY;
        if (debug) console.log(`Asteroid (${x}, ${y}) is (${vX}, ${vY}) from station (${stationX}, ${stationY})`);
        if (typeof asteroids[id] == 'undefined') {
          let quadrant;
          if (vX >= 0 && vY < 0) {
            quadrant = 1;
          } else if (vX >= 0 && vY >= 0) {
            quadrant = 2;
          } else if (vX < 0 && vY >= 0) {
            quadrant = 3;
          } else {
            quadrant = 4;
          }
          asteroids[id] = {
            vector: [vX, vY],
            quadrant: quadrant,
            angle: Math.atan((-1 * vY) / vX),
            asteroids: []
          };
        }
        asteroids[id]['asteroids'].push([x, y]);
      }
    }
  }
  if (debug) console.log(asteroids);
  let sorted = [];
  Object.keys(asteroids).sort((a, b) => {
    if (asteroids[a].quadrant != asteroids[b].quadrant) {
      return asteroids[a].quadrant - asteroids[b].quadrant;
    }
    return asteroids[b].angle - asteroids[a].angle;
  }).forEach(id => {
    let oid = asteroids[id];
    oid.asteroids = oid.asteroids.sort((a , b) => {
      let aDist = calcDistance(stationX, stationY, a[0], a[1]);
      let bDist = calcDistance(stationX, stationY, b[0], b[1]);
      if (debug) console.log(`Asteroid (${a[0]}, ${a[1]}) is ${aDist} from the station, while (${b[0]}, ${b[1]}) is ${bDist}`);
      return aDist - bDist;
    });
    sorted.push(asteroids[id]);
  });
  if (debug) console.log(sorted);

  let vaporized = [];
  while(sorted.length > 0) {
    // Loop through list, taking one asteroid off each vector set
    for (let i = 0; i < sorted.length; i++) {
      vaporized.push(sorted[i].asteroids.shift());
    }
    sorted = sorted.filter(oid => oid.asteroids.length > 0);
  }
  return vaporized;
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'find-best': {
      let rs = findBest(e.data.map);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'vaporized-order': {
      let rs = vaporizedOrder(e.data.map, e.data.x, e.data.y);
      self.postMessage({
        id: e.data.id,
        result:rs
      });
    }
  }
};
