function pointId([x, y]) {
  return ('000000' + x).slice(-6) + ':' + ('000000' + y).slice(-6);
}

function getDeltas(direction) {
  let xDelta = 0, yDelta = 0;
  switch (direction) {
    case 'U':
      yDelta =  -1;
      break;
    case 'D':
      yDelta = 1;
      break;
    case 'L':
      xDelta = -1;
      break;
    case 'R':
      xDelta = 1;
      break;
  }
  return [xDelta, yDelta];
}

function crossedWirePoint(wires, debug = false) {
  let passedPoints = {};
  let position = [0, 0];
  if (debug) console.groupCollapsed('Wire set');
  wires[0].forEach(instruction => {
    const direction = instruction[0];
    const distance = parseInt(instruction.slice(1));
    let [xDelta, yDelta] = getDeltas(direction);
    for (let i = 0; i < distance; i++) {
      position[0] += xDelta;
      position[1] += yDelta;
      if (debug) console.log(instruction, i, pointId(position), 'passed by wire 1');
      passedPoints[pointId(position)] = true;
    }
  });

  let crosses = [];
  position = [0, 0];
  wires[1].forEach(instruction => {
    const direction = instruction[0];
    const distance = parseInt(instruction.slice(1));
    let [xDelta, yDelta] = getDeltas(direction);
    for (let i = 0; i < distance; i++) {
      position[0] += xDelta;
      position[1] += yDelta;
      let id = pointId(position);
      if (passedPoints[id] === true) {
        if (debug) console.log(instruction, i, id, 'crossed point');
        crosses.push([position[0], position[1]]);
      } else {
        if (debug) console.log(instruction, i, id, 'not a cross');
      }
    }
  });
  if (debug) console.groupEnd();
  return crosses;
}

function getSignalLength(point, wire) {
  let position = [0,0];
  let length = 0;
  for (let i = 0; i < wire.length; i++) {
    const instruction = wire[i];
    const direction = instruction[0];
    const distance = parseInt(instruction.slice(1));
    let [xDelta, yDelta] = getDeltas(direction);
    for (let i = 0; i < distance; i++) {
      position[0] += xDelta;
      position[1] += yDelta;
      length++;
      if (position[0] == point[0] && position[1] == point[1]) {
        return length;
      }
    }
  }
  return false;
}

function crossedWireDistance(wires) {
  let crosses = crossedWirePoint(wires);
  if (crosses.length === 0) return false;

  crosses = crosses.map(point => {
    return Math.abs(point[0]) + Math.abs(point[1]);
  }).sort((a, b) => {
    return a - b;
  });

  return crosses[0];
}

function crossedWireSignal(wires) {
  return new Promise((resolve, reject) => {
    let crosses = crossedWirePoint(wires);
    if (crosses.length === 0) {
      reject('No crossing points found');
      return;
    }

    // Spawn workers to calculate signal lengths
    let signalPromises = crosses.map((point, index) => {
      let signalA = doWork({
        id: index + ':A',
        cmd: 'signal-length',
        point: point,
        wire: wires[0]
      });
      let signalB = doWork({
        id: index + ':B',
        cmd: 'signal-length',
        point: point,
        wire: wires[1]
      });
      return Promise.all([signalA, signalB]).then(rs => {
        return rs[0].result + rs[1].result;
      });
    });

    Promise.all(signalPromises).then(signalSums => {
      let sorted = signalSums.sort((a, b) => {
        return a - b;
      });
      resolve(sorted[0]);
    });
  });
}

function doWork(message) {
  return new Promise((resolve, reject) => {
    let worker = new Worker('03-worker.js');
    worker.onmessage = function(e) {
      resolve(e.data);
      //worker.terminate();
    };
    worker.onerror = function(e) {
      reject(e.data);
      //worker.terminate();
    };
    worker.postMessage(message);
  });
}

self.onmessage = function(e) {
  switch (e.data.cmd) {
    case 'crossed-wire-distance': {
      let rs = crossedWireDistance(e.data.wires);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
    case 'crossed-wire-signal': {
      crossedWireSignal(e.data.wires).then(rs => {
        self.postMessage({
          id: e.data.id,
          result: rs
        });
      });
      break;
    }
    case 'signal-length': {
      let rs = getSignalLength(e.data.point, e.data.wire);
      self.postMessage({
        id: e.data.id,
        result: rs
      });
      break;
    }
  }
};
