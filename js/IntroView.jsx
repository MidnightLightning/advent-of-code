import React from 'react';

class IntroView extends React.Component {
  render() {
    return (
      <section>
        <p>Hello!</p>
      </section>
    );
  }
}

export default IntroView;
