import React from 'react';
import UnitTests from 'UnitTests';
import ButtonBar from 'ButtonBar';

/**
 * Given a set of test cases and their async results, combine into a UI view
 * Takes the "label" and "expected" values from the "case" object, and the "actual" from the results object.
 */
function showAsyncTestResults(testCases, results) {
  if (results === false || results === null) return;
  let unitTests = testCases.map((test, index) => {
    return {
      label: test.label,
      expected: test.expected,
      actual: results[index]
    };
  });
  return (
    <UnitTests tests={unitTests} />
  );
}
function AsyncTestResults({ testCases, results }) {
  let rs = showAsyncTestResults(testCases, results);
  if (typeof rs == 'undefined') {
    return (<span />);
  }
  return rs;
}
function OptionalValue({ value }) {
  if (typeof value == 'undefined' || value === false || value == null) {
    return (<span />);
  }
  return (
    <div>{value}</div>
  );
}
function OptionalView({ view }) {
  if (typeof view == 'undefined' || view === false || view == null) {
    return (<span />);
  }
  return view;
}

function logPerformanceEnd(startMark, endMark, clearWhenDone = true) {
  performance.mark(endMark);
  performance.measure('work-time', startMark, endMark);
  let timing = performance.getEntriesByName('work-time')[0].duration;
  console.log('Processing time (in ms)', timing);
  if (clearWhenDone) {
    performance.clearMarks();
    performance.clearMeasures();
  }
}

function buildPuzzlePart(inputOpts, label) {
  const opts = Object.assign({
    testCases: [],
    testCaseMapper: function(testCase) {
      return Promise.resolve(true);
    },

    solveImmediate: false,
    solveHandler: function(data) {
      return Promise.resolve(true);
    }
  }, inputOpts);
  class PuzzlePart extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        testResults: false,
        puzzleSolution: false,
        testCases: opts.testCases,
        extraView: false
      };
    }

    componentDidMount() {
      Promise.all(this.state.testCases.map(opts.testCaseMapper))
        .then(rs => {
          this.setState(curState => {
            curState.testResults = rs.map(row => row.result);
            return curState;
          });
        });

      let data = this.props.data;
      if (typeof opts.dataParser == 'function') {
        data = opts.dataParser(data);
      }

      if (opts.solveImmediate) {
        opts.solveHandler(data).then(rs => {
          this.setState(curState => {
            curState.puzzleSolution = rs.result;
            return curState;
          });
        });
      }

      if (typeof opts.extraView == 'function') {
        opts.extraView(data).then(rs => {
          this.setState(curState => {
            curState.extraView = rs;
            return curState;
          });
        });
      }
    }

    componentWillUnmount() {
      if (typeof opts.cleanup == 'function') {
        opts.cleanup();
      }
    }

    doWork() {
      performance.mark('work-start');
      let data = this.props.data;
      if (typeof opts.dataParser == 'function') {
        data = opts.dataParser(data);
      }
      opts.solveHandler(data).then(rs => {
        logPerformanceEnd('work-start', 'work-end');
        this.setState(curState => {
          curState.puzzleSolution = rs;
          return curState;
        });
      });
    }

    render() {
      let extraView;
      if (typeof opts.extraView == 'function') {
        extraView = (
          <OptionalView view={this.state.extraView} />
        );
      }

      let solver;
      if (opts.solveImmediate) {
        solver = (
          <OptionalValue value={this.state.puzzleSolution} />
        );
      } else {
        solver = (
          <React.Fragment>
            <p>
              <button onClick={e => this.doWork()}>Run</button>
            </p>
            <OptionalValue value={this.state.puzzleSolution} />
          </React.Fragment>
        );
      }
      return (
        <div>
          <h1>Part {label}</h1>
          <AsyncTestResults testCases={this.state.testCases} results={this.state.testResults} />
          {extraView}
          {solver}
        </div>
      );
    }
  }

  PuzzlePart.displayName = `PuzzlePart${label})`;
  return PuzzlePart;
}

function buildPuzzleView({ partOne, partTwo, dataUrl }) {
  function PuzzleView(props) {
    let partName = (props.navPath[1] == 'part-2') ? 'Two' : 'One';
    let currentPartOpts = (props.navPath[1] == 'part-2') ? partTwo : partOne;
    let BodyContent = buildPuzzlePart(currentPartOpts, partName);
    return (
      <section>
        <ButtonBar base={props.navPath[0]} />
        <BodyContent data={props.data} />
      </section>
    );
  }

  return withData(PuzzleView, dataUrl);
}

function withData(PuzzleView, dataUrl) {
  class DataLoaderView extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: false
      };

      this.updateData = this.updateData.bind(this);
    }

    componentDidMount() {
      this.updateData();
    }

    updateData() {
      if (typeof dataUrl == 'undefined') return;
      fetch(dataUrl).then(rs => {
        if (rs.ok == false) {
          console.error(rs);
          return;
        }
        rs.text().then(data => {
          this.setState(curState => {
            curState.data = data.split("\n");
            return curState;
          });
        });
      });
    }

    render() {
      if (this.state.data == false) {
        return (
          <section>Loading...</section>
        );
      }

      return (
        <PuzzleView data={this.state.data} {...this.props} />
      );
    }

  }

  let childName = PuzzleView.displayName || PuzzleView.name || 'Component';
  DataLoaderView.displayName = `DataLoaderView(${childName})`;
  return DataLoaderView;
}

export {
  showAsyncTestResults,
  AsyncTestResults,
  OptionalValue,
  logPerformanceEnd,
  withData,
  buildPuzzleView
};
