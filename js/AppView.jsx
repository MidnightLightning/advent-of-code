import React from 'react';
import dispatcher from 'dispatcher';
import NavView from 'NavView';
import navData from 'navData';

let routes = {};
navData.forEach(section => {
  section.routes.forEach(route => {
    routes[route.id] = route.controller;
  });
});

class AppView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: false,
      pageClass: false,
      listeners: []
    };

    this.handleNavigateTo = this.handleNavigateTo.bind(this);
  }

  componentDidMount() {
    let navigationListener = dispatcher.subscribe('navigateTo', this.handleNavigateTo);
    this.setState(curState => {
      curState.listeners = [navigationListener];
      return curState;
    });

    let jumpTo = window.location.hash.substr(1);
    if (jumpTo == '') {
      // No destination given; use default
      jumpTo = 'intro';
    }
    this.handleNavigateTo(jumpTo);
  }

  componentWillUnmount() {
    this.state.listeners.forEach(listener => {
      listener.unsubscribe();
    });
  }

  handleNavigateTo(newPath) {
    window.location.hash = newPath;
    if (newPath === this.state.currentPage) return;
    const navPath = newPath.split(':');
    const classFilePath = routes[navPath[0]];
    // Use Babel 'conditional import' style to only load body class now that navigation has been selected
    import(classFilePath).then(BodyClass => {
      this.setState(curState => {
        curState.currentPage = newPath;
        curState.pageClass = BodyClass.default;
        return curState;
      });
    });
  }

  render() {
    if (this.state.currentPage === false) {
      return <p>Loading...</p>;
    }
    let navPath = this.state.currentPage.split(':');
    let pageProps = {
      navPath: navPath
    };
    return (
      <div id="app-container">
        <NavView {...pageProps} />
        <this.state.pageClass {...pageProps} />
      </div>
    );
  }
}

export default AppView;
