import React from 'react';
import ReactDOM from 'react-dom';
import AppView from './AppView';

const domContainer = document.getElementById('app');
ReactDOM.render(<AppView />, domContainer);
