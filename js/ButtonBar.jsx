import React from 'react';
import dispatcher from 'dispatcher';


class ButtonBar extends React.Component {
  render() {
    const baseNav = this.props.base;
    return (
      <p>
        <button
          onClick={e => dispatcher.publish('navigateTo', baseNav)}
          style={{ marginRight: '0.5rem' }}
        >Part One</button>
        <button
          onClick={e => dispatcher.publish('navigateTo', baseNav + ':part-2')}
        >Part Two</button>
      </p>
    )
  }
}

export default ButtonBar;
