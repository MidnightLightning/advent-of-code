const navData = [
  {
    label: 'Main',
    routes: [
      {
        id: 'intro',
        label: 'Intro',
        controller: 'IntroView'
      },
    ]
  },
  {
    label: '2019',
    routes: [
      {
        id: '2019-01',
        label: '01. The Tyranny of the Rocket Equation',
        controller: '2019/01-TyrannyOfTheRocketEquation'
      },
      {
        id: '2019-02',
        label: '02. 1202 Program Alarm',
        controller: '2019/02-ProgramAlarm'
      },
      {
        id: '2019-03',
        label: '03. Crossed Wires',
        controller: '2019/03-CrossedWires'
      },
      {
        id: '2019-04',
        label: '04. Secure Container',
        controller: '2019/04-SecureContainer'
      },
      {
        id: '2019-05',
        label: '05. Sunny with a Chance of Asteroids',
        controller: '2019/05-SunnyAsteroids'
      },
      {
        id: '2019-06',
        label: '06. Universal Orbit Map',
        controller: '2019/06-UniversalOrbitMap'
      },
      {
        id: '2019-07',
        label: '07. Amplification Circuit',
        controller: '2019/07-AmplificationCircuit'
      },
      {
        id: '2019-08',
        label: '08. Space Image Format',
        controller: '2019/08-SpaceImageFormat'
      },
      {
        id: '2019-09',
        label: '09. Sensor Boost',
        controller: '2019/09-SensorBoost'
      },
      {
        id: '2019-10',
        label: '10. Monitoring Station',
        controller: '2019/10-MonitoringStation'
      },
      {
        id: '2019-11',
        label: '11. Space Police',
        controller: '2019/11-SpacePolice'
      },
      {
        id: '2019-12',
        label: '12. N-Body Problem',
        controller: '2019/12-NBodyProblem'
      },
      {
        id: '2019-13',
        label: '13. Care Package',
        controller: '2019/13-CarePackage'
      },
      {
        id: '2019-17',
        label: '17. Set and Forget',
        controller: '2019/17-SetAndForget'
      },
      {
        id: '2019-19',
        label: '19. Tractor Beam',
        controller: '2019/19-TractorBeam'
      },
      {
        id: '2019-25',
        label: '25. Cryostasis',
        controller: '2019/25-Cryostasis'
      }
    ]
  },
  {
    label: '2020',
    routes: [
      {
        id: '2020-01',
        label: '01. Report Repair',
        controller: '2020/01-ReportRepair'
      },
      {
        id: '2020-02',
        label: '02. Password Philosophy',
        controller: '2020/02-PasswordPhilosophy'
      },
      {
        id: '2020-03',
        label: '03. Toboggan Trajectory',
        controller: '2020/03-TobogganTrajectory'
      },
      {
        id: '2020-04',
        label: '04. Passport Processing',
        controller: '2020/04-PassportProcessing'
      },
      {
        id: '2020-05',
        label: '05. Binary Boarding',
        controller: '2020/05-BinaryBoarding'
      },
      {
        id: '2020-06',
        label: '06. Custom Customs',
        controller: '2020/06-CustomCustoms'
      },
      {
        id: '2020-07',
        label: '07. Handy Haversacks',
        controller: '2020/07-HandyHaversacks'
      },
      {
        id: '2020-08',
        label: '08. Handheld Halting',
        controller: '2020/08-HandheldHalting'
      },
      {
        id: '2020-09',
        label: '09. Encoding Error',
        controller: '2020/09-EncodingError'
      },
      {
        id: '2020-10',
        label: '10. Adapter Array',
        controller: '2020/10-AdapterArray'
      },
      {
        id: '2020-11',
        label: '11. Seating System',
        controller: '2020/11-SeatingSystem'
      },
      {
        id: '2020-12',
        label: '12. Rain Risk',
        controller: '2020/12-RainRisk'
      },
      {
        id: '2020-13',
        label: '13. Shuttle Search',
        controller: '2020/13-ShuttleSearch'
      },
      {
        id: '2020-14',
        label: '14. Docking Data',
        controller: '2020/14-DockingData'
      },
      {
        id: '2020-15',
        label: '15. Rambunctious Recitation',
        controller: '2020/15-RambunctiousRecitation'
      },
      {
        id: '2020-16',
        label: '16. Ticket Translation',
        controller: '2020/16-TicketTranslation'
      },
      {
        id: '2020-17',
        label: '17. Conway Cubes',
        controller: '2020/17-ConwayCubes'
      }
    ]
  }
];

export default navData;
