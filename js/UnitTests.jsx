import React from 'react';

function parseInput(originalExpected, originalActual) {
  let expected, actual;
  if (Array.isArray(originalExpected)) {
    expected = originalExpected.join(', ');
    if (Array.isArray(originalActual)) {
      actual = originalActual.join(', ');
    } else {
      actual = originalActual;
    }
    return { expected, actual };
  }
  if (typeof originalExpected == 'object' && Object.keys(originalExpected).length > 0) {
    expected = JSON.stringify(originalExpected);
    actual = JSON.stringify(originalActual);
    return { expected, actual };
  }
  return {
    expected: originalExpected,
    actual: originalActual
  };
}

class UnitTests extends React.Component {
  render() {
    const tests = this.props.tests;
    if (tests.length == 0) {
      return (
        <div>
          <p style={{ fontStyle: 'italic' }}>No tests defined!</p>
        </div>
      );
    }

    const successStyle = { backgroundColor: '#CFC' },
      failStyle = { backgroundColor: '#FCC' };

    return (
      <div className="unit-tests">
        {
          tests.map((test, index) => {
            let label = (typeof test.label == 'undefined') ? test.actual : test.label;
            let { expected, actual } = parseInput(test.expected, test.actual);

            if (expected === true) {
              expected = 'true';
            } else if (expected === false) {
              expected = 'false';
            }

            if (actual === true) {
              actual = 'true';
            } else if (actual === false) {
              actual = 'false';
            }

            if (expected == actual) {
              return (
                <p key={index} style={successStyle}>{label} &rArr; {actual}</p>
              );
            }

            return (
              <p key={index} style={failStyle}>{label} &rArr; {actual} ({expected})</p>
            );
          })
        }
      </div>
    );
  }
}

export default UnitTests;
