
function counterFactory() {
  let lastId = 1000;
  return function getNextId() {
    lastId += 1;
    return lastId;
  };
}
const idGenerator = counterFactory();

class EventBus {
  constructor() {
    this.subscriptions = {};
    this.lastId = 1000;
  }

  subscribe(eventType, callback) {
    if (typeof callback !== 'function') {
      console.error(`Callback passed to subscribe function is a ${typeof callback}, not a function`);
      return;
    }
    const id = idGenerator();
    if (typeof this.subscriptions[eventType] === 'undefined') {
      this.subscriptions[eventType] = {};
    }
    this.subscriptions[eventType][id] = callback;

    return {
      unsubscribe: () => {
        delete this.subscriptions[eventType][id];
        if (Object.keys(this.subscriptions[eventType]).length === 0) delete this.subscriptions[eventType];
      }
    };
  }

  publish(eventType, arg) {
    if (typeof this.subscriptions[eventType] == 'undefined') return;
    Object.keys(this.subscriptions[eventType]).forEach(key => this.subscriptions[eventType][key](arg));
  }
}

export default new EventBus();
