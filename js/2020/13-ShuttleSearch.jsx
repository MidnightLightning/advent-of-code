import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/13-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile), 'Root');

const exampleData = [
  '939',
  '7,13,x,x,59,x,31,19'
];

function parseData(input) {
  return {
    startTime: parseInt(input[0]),
    buses: input[1].split(',')
  };
}

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 295, input: parseData(exampleData) }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: parseData,
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 1068781, input: parseData(exampleData) },
      { label: '17,x,13,19', expected: 3417, input: { buses: '17,x,13,19'.split(',') } },
      { label: '67,7,59,61', expected: 754018, input: { buses: '67,7,59,61'.split(',') } },
      { label: '67,x,7,59,61', expected: 779210, input: { buses: '67,x,7,59,61'.split(',') } },
      { label: '67,7,x,59,61', expected: 1261476, input: { buses: '67,7,x,59,61'.split(',') } },
      {
        label: '1789,37,47,1889',
        expected: 1202161486,
        input: {
          buses: '1789,37,47,1889'.split(',')
        }
      },
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: parseData,
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/13-data.txt'
});
