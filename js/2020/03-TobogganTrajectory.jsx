import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/03-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleMap = [
  '..##.......',
  '#...#...#..',
  '.#....#..#.',
  '..#.#...#.#',
  '.#...##..#.',
  '..#.##.....',
  '.#.#.#....#',
  '.#........#',
  '#.##...#...',
  '#...##....#',
  '.#..#...#.#'
];

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 7, input: exampleMap }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: '1, 1', expected: 2, map: exampleMap, slope: [1, 1] },
      { label: '3, 1', expected: 7, map: exampleMap, slope: [3, 1] },
      { label: '5, 1', expected: 3, map: exampleMap, slope: [5, 1] },
      { label: '7, 1', expected: 4, map: exampleMap, slope: [7, 1] },
      { label: '1, 2', expected: 2, map: exampleMap, slope: [1, 2] },
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', map: testCase.map, slope: testCase.slope });
    },
    solveHandler: function(data) {
      return Promise.all([
        workerQueue.send({ cmd: 'part-two', map: data, slope: [1, 1] }),
        workerQueue.send({ cmd: 'part-two', map: data, slope: [3, 1] }),
        workerQueue.send({ cmd: 'part-two', map: data, slope: [5, 1] }),
        workerQueue.send({ cmd: 'part-two', map: data, slope: [7, 1] }),
        workerQueue.send({ cmd: 'part-two', map: data, slope: [1, 2] })
      ]).then(rs => {
        let product = rs.reduce((subtotal, record) => subtotal * record.result, 1);
        return product;
      });
    }
  },
  dataUrl: '/data/2020/03-data.txt'
});
