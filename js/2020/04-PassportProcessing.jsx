import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/04-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleBatch = [
  'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
  'byr:1937 iyr:2017 cid:147 hgt:183cm',
  '',
  'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884',
  'hcl:#cfa07d byr:1929',
  '',
  'hcl:#ae17e1 iyr:2013',
  'eyr:2024',
  'ecl:brn pid:760753108 byr:1931',
  'hgt:179cm',
  '',
  'hcl:#cfa07d eyr:2025 pid:166559648',
  'iyr:2011 ecl:brn hgt:59in',
];

const validBatch = [
  'pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980',
  'hcl:#623a2f',
  '',
  'eyr:2029 ecl:blu cid:129 byr:1989',
  'iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm',
  '',
  'hcl:#888785',
  'hgt:164cm byr:2001 iyr:2015 cid:88',
  'pid:545766238 ecl:hzl',
  'eyr:2022',
  '',
  'iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719'
];

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 2, input: exampleBatch }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    },
    extraView: function(data) {
      return workerQueue.send({
        cmd: 'parse-text',
        arg: data
      }).then(rs => {
        let colorRegex = /^#[a-f0-9]{6}$/;
        let passports = rs.result.filter(passport => {
          if (typeof passport['hcl'] == 'undefined') return false;
          if (passport.hcl.match(colorRegex) == null) return false;
          return true;
        });
        return (
          <div style={{ display: 'flex', flexWrap: 'wrap', margin: '2rem 0' }}>
            {
              passports.map(passport => {
                const r = parseInt(passport.hcl.substr(1, 2), 16);
                const g = parseInt(passport.hcl.substr(3, 2), 16);
                const b = parseInt(passport.hcl.substr(5, 2), 16);
                const sum = r + b + g;
                let fontColor = (sum > 125) ? 'black' : 'white';
                const style = {
                  flex: '0 0 75px',
                  padding: '0.2rem 0.5rem',
                  textAlign: 'center',
                  backgroundColor: passport.hcl,
                  fontFamily: 'monospace',
                  color: fontColor
                };
                return (
                  <div style={style}>
                    {passport.hcl}
                  </div>
                );
              })
            }
          </div>
        );
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 4, input: validBatch }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/04-data.txt'
});
