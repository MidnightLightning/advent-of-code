import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/08-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleProgram = [
  'nop +0',
  'acc +1',
  'jmp +4',
  'acc +3',
  'jmp -3',
  'acc -99',
  'acc +1',
  'jmp -4',
  'acc +6'
];

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 5, input: exampleProgram }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 8, input: exampleProgram }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: function(data) {
      return data.filter(line => {
        return line !== '';
      });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/08-data.txt'
});
