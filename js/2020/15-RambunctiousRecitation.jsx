import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/15-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

function noBlanks(data) {
  return data.filter(line => {
    return line !== '';
  })[0];
}

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: '0,3,6', expected: 436, input: '0,3,6' },
      { label: '1,3,2', expected: 1, input: '1,3,2' },
      { label: '2,1,3', expected: 10, input: '2,1,3' },
      { label: '1,2,3', expected: 27, input: '1,2,3' },
      { label: '2,3,1', expected: 78, input: '2,3,1' },
      { label: '3,2,1', expected: 438, input: '3,2,1' },
      { label: '3,1,2', expected: 1836, input: '3,1,2' }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: noBlanks,
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: '0,3,6', expected: 175594, input: '0,3,6' },
      { label: '1,3,2', expected: 2578, input: '1,3,2' },
      { label: '2,1,3', expected: 3544142, input: '2,1,3' },
      { label: '1,2,3', expected: 261214, input: '1,2,3' },
      { label: '2,3,1', expected: 6895259, input: '2,3,1' },
      { label: '3,2,1', expected: 18, input: '3,2,1' },
      { label: '3,1,2', expected: 362, input: '3,1,2' }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: noBlanks,
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/15-data.txt'
});
