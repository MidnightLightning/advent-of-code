import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const exampleList = [
  1721,
  979,
  366,
  299,
  675,
  1456
];

const workerFile = 'js/workers/2020/01-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 514579, input: exampleList }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', numbers: testCase.input });
    },
    solveImmediate: true,
    dataParser: function(data) {
      return data.map(row => parseInt(row));
    },
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        numbers: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 241861950, input: exampleList }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', numbers: testCase.input });
    },
    dataParser: function(data) {
      return data.map(row => parseInt(row));
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', numbers: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/01-data.txt'
});
