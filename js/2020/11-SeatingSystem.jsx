import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/11-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleMap = [
  'L.LL.LL.LL',
  'LLLLLLL.LL',
  'L.L.L..L..',
  'LLLL.LL.LL',
  'L.LL.LL.LL',
  'L.LLLLL.LL',
  '..L.L.....',
  'LLLLLLLLLL',
  'L.LLLLLL.L',
  'L.LLLLL.LL'
];

function noBlanks(data) {
  return data.filter(line => {
    return line !== '';
  });
}


export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 37, input: exampleMap }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: noBlanks,
    solveImmediate: false,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      }).then(rs => rs.result);
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 26, input: exampleMap }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/11-data.txt'
});
