import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/10-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleAdapters = [
  16,
  10,
  15,
  5,
  1,
  11,
  7,
  19,
  6,
  12,
  4,
];

const largerExample = [
  28,
  33,
  18,
  42,
  31,
  14,
  46,
  20,
  48,
  47,
  24,
  23,
  49,
  45,
  19,
  38,
  39,
  11,
  1,
  32,
  25,
  35,
  8,
  17,
  7,
  9,
  4,
  2,
  34,
  10,
  3,
];

const ex = [
  [10, 6, 4, 7, 1, 5],
  [4, 11, 7, 8, 1, 6, 5],
  [3, 1, 6, 2],
  [17, 6, 10, 5, 13, 7, 1, 4, 12, 11, 14],
];

/*
## Example 1
Has **4** possible combinations:

```
10
6
4
7
1
5
```

## Example 2
Has **7** possible combinations:

```
4
11
7
8
1
6
5
```

## Example 3
Has **4** possible combinations:

```
3
1
6
2
```

## Example 4
Has **28** possible combinations:

```
17
6
10
5
13
7
1
4
12
11
14
```
*/

function intData(data) {
  return data.filter(line => {
    return line !== '';
  }).map(line => {
    return parseInt(line);
  });
}

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: { 1: 7, 3: 5 }, input: exampleAdapters }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'parse-list', arg: testCase.input });
    },
    dataParser: intData,
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example Deltas', expected: [1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3], cmd: 'deltas', input: exampleAdapters },
      { label: 'Example Count', expected: 8, cmd: 'part-two', input: exampleAdapters },
      {
        label: 'XL Deltas',
        expected: [1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 3, 3, 1, 1, 1, 1, 3, 1, 3, 3, 1, 1, 1, 1],
        cmd: 'deltas',
        input: largerExample
      },
      { label: 'XL Count', expected: 19208, cmd: 'part-two', input: largerExample },

      { label: 'Small Deltas', expected: [1, 3, 1, 1, 1, 3], cmd: 'deltas', input: ex[0] },
      { label: 'Small Count', expected: 4, cmd: 'part-two', input: ex[0] },
      { label: 'Medium Deltas', expected: [1, 3, 1, 1, 1, 1, 3], cmd: 'deltas', input: ex[1] },
      { label: 'Medium Count', expected: 7, cmd: 'part-two', input: ex[1] },
      { label: 'Sneaky Deltas', expected: [1, 1, 1, 3], cmd: 'deltas', input: ex[2] },
      { label: 'Sneaky Count', expected: 4, cmd: 'part-two', input: ex[2] },
      {
        label: 'Large Deltas',
        expected: [1, 3, 1, 1, 1, 3, 1, 1, 1, 1, 3],
        cmd: 'deltas',
        input: ex[3]
      },
      { label: 'Large Count', expected: 28, cmd: 'part-two', input: ex[3] },
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: testCase.cmd, arg: testCase.input });
    },
    dataParser: intData,
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/10-data.txt'
});
