import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/16-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleTickets = [
  'class: 1-3 or 5-7',
  'row: 6-11 or 33-44',
  'seat: 13-40 or 45-50',
  '',
  'your ticket:',
  '7,1,14',
  '',
  'nearby tickets:',
  '7,3,47',
  '40,4,50',
  '55,2,20',
  '38,6,12'
];

const exampleTickets2 = [
  'class: 0-1 or 4-19',
  'row: 0-5 or 8-19',
  'seat: 0-13 or 16-19',
  '',
  'your ticket:',
  '11,12,13',
  '',
  'nearby tickets:',
  '3,9,18',
  '15,1,5',
  '5,14,9'
];

function parseRule(rule) {
  let colonPos = rule.indexOf(': ');
  let propName = rule.substr(0, colonPos);
  let value = rule.substr(colonPos + 2);
  value = value.split(' or ').map(range => {
    let [min, max] = range.split('-').map(n => parseInt(n));
    return { min, max };
  });
  return {
    propName,
    value
  };
}

function parseTicketData(input) {
  let firstBlank = input.indexOf('');
  let rules = input.slice(0, firstBlank).map(parseRule);
  let myTicket = input[firstBlank + 2].split(',').map(n => parseInt(n));
  let otherTickets = input.slice(firstBlank + 5).filter(line => {
    return line !== '';
  }).map(ticket => {
    return ticket.split(',').map(n => parseInt(n));
  });
  return {
    rules,
    myTicket,
    otherTickets
  };
}

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 71, input: parseTicketData(exampleTickets) }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: parseTicketData,
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: { seat: 13, class: 12, row: 11 }, input: parseTicketData(exampleTickets2) }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: parseTicketData,
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => {
        return (
          <React.Fragment>
            {
              Object.keys(rs.result).map(propName => {
                return (
                  <p key={propName}><strong>{propName}</strong> {rs.result[propName]}</p>
                );
              })
            }
          </React.Fragment>
        );
      });
    }
  },
  dataUrl: '/data/2020/16-data.txt'
});
