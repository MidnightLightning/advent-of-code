import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/12-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleDirections = [
  'F10',
  'N3',
  'F7',
  'R90',
  'F11'
];

function noBlanks(data) {
  return data.filter(line => {
    return line !== '';
  });
}


export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 25, input: exampleDirections }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: noBlanks,
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 286, input: exampleDirections }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: noBlanks,
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/12-data.txt'
});
