import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/17-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: '111111', expected: true, input: '111111' }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: '111111', expected: true, input: '111111' }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/17-data.txt'
});
