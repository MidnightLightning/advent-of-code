import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/05-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'FBFBBFFRLR', expected: { row: 44, column: 5, id: 357 }, seat: 'FBFBBFFRLR', cmd: 'parse-seat' },
      { label: 'BFFFBBFRRR', expected: { row: 70, column: 7, id: 567 }, seat: 'BFFFBBFRRR', cmd: 'parse-seat' },
      { label: 'FFFBBBFRRR', expected: { row: 14, column: 7, id: 119 }, seat: 'FFFBBBFRRR', cmd: 'parse-seat' },
      { label: 'BBFFBBFRLL', expected: { row: 102, column: 4, id: 820 }, seat: 'BBFFBBFRLL', cmd: 'parse-seat' }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: testCase.cmd, arg: testCase.seat });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: data
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'FBFBBFFRLR', expected: { row: 44, column: 5, id: 357 }, seat: 'FBFBBFFRLR', cmd: 'parse-seat' },
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: testCase.cmd, arg: testCase.seat });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'find-hole', arg: data }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/05-data.txt'
});
