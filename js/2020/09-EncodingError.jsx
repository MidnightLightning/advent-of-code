import React from 'react';
import { WorkerQueue } from 'workerUtils';
import { buildPuzzleView } from 'puzzleUtils';

const workerFile = 'js/workers/2020/09-worker.js';
const workerQueue = new WorkerQueue(new Worker(workerFile));

const exampleStream = [
  35,
  20,
  15,
  25,
  47,
  40,
  62,
  55,
  65,
  95,
  102,
  117,
  150,
  182,
  127,
  219,
  299,
  277,
  309,
  576,
];

export default buildPuzzleView({
  partOne: {
    testCases: [
      { label: 'Example', expected: 127, input: { stream: exampleStream, windowSize: 5 } }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'solve-puzzle', arg: testCase.input });
    },
    dataParser: function(data) {
      return data.filter(line => {
        return line !== '';
      }).map(line => {
        return parseInt(line);
      });
    },
    solveImmediate: true,
    solveHandler: function(data) {
      return workerQueue.send({
        cmd: 'solve-puzzle',
        arg: { stream: data, windowSize: 25 }
      });
    }
  },
  partTwo: {
    testCases: [
      { label: 'Example', expected: 62, input: { stream: exampleStream, windowSize: 5 } }
    ],
    testCaseMapper: function(testCase) {
      return workerQueue.send({ cmd: 'part-two', arg: testCase.input });
    },
    dataParser: function(data) {
      return data.filter(line => {
        return line !== '';
      }).map(line => {
        return parseInt(line);
      });
    },
    solveHandler: function(data) {
      return workerQueue.send({ cmd: 'part-two', arg: { stream: data, windowSize: 25 } }).then(rs => rs.result);
    }
  },
  dataUrl: '/data/2020/09-data.txt'
});
