
const resolves = {};
const rejects = {};
let globalMessageId = 0;

function handleMessage(message) {
  const data = message.data;
  if (typeof data.err !== 'undefined' && data.err !== 'ok') {
    // Error state
    const reject = rejects[data.id];
    if (typeof reject !== 'function') {
      console.error('Cannot call reject function!!');
      return;
    }
    reject(data);
  } else {
    // Success state
    const resolve = resolves[data.id];
    if (typeof resolve !== 'function') {
      console.error('Cannot call resolve function!!');
      return;
    }
    resolve(data);
  }

  delete resolves[data.id];
  delete rejects[data.id];
}

/**
 * Turn multiple calls to a single worker into a series of Promises.
 * https://codeburst.io/promises-for-the-web-worker-9311b7831733
 */
class WorkerQueue {
  constructor(worker, label) {
    this.worker = worker;
    this.label = label;
    this.worker.onmessage = handleMessage;
  }

  send(message) {
    message.id = globalMessageId++;
    message.workerLabel = this.label;
    return new Promise((resolve, reject) => {
      // Save callbacks for later
      resolves[message.id] = resolve;
      rejects[message.id] = reject;

      this.worker.postMessage(message);
    });
  }

  terminate() {
    this.worker.terminate();
  }
}

export {
  WorkerQueue
};
