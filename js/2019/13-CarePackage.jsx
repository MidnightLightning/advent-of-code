import React from 'react';
import ButtonBar from 'ButtonBar';
import { logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute }  from '2019/intcodeProcessor';

function chunkArray(arr, chunkLength) {
  let tmp = [];
  for (let i = 0; i < arr.length; i+=chunkLength) {
    tmp.push(arr.slice(i, i + chunkLength));
  }
  return tmp;
}

function runGame(memory, onDraw) {
  let state = {
    score: false,
    outputLog: [],
    board: {},
    ballPos: false,
    paddlePos: false,
    minX: 0,
    maxX: 0,
    minY: 0,
    maxY: 0
  };
  let keyState = {};
  window.addEventListener('keyup', e => keyState[e.key] = false);
  window.addEventListener('keydown', e => keyState[e.key] = true);

  memory[0] = 2; // Free play!

  function parseInput(count) {
    //console.log(count);

    if (state.ballPos !== false && state.paddlePos !== false) {
      if (state.ballPos[0] < state.paddlePos[0]) {
        return -1;
      }
      if (state.ballPos[0] > state.paddlePos[0]) {
        return 1;
      }
      return 0;
    }
    /*
    if (keyState['ArrowRight'] == true) {
      return 1;
    }
    if (keyState['ArrowLeft'] == true) {
      return -1;
    }
    */
    return 0;
  }
  function parseOutput(data) {
    state.outputLog.push(data);
    if (state.outputLog.length < 3) return;

    const tile = {
      x: state.outputLog[0],
      y: state.outputLog[1],
      id: state.outputLog[2]
    };
    state.outputLog = [];
    if (tile.x == -1 && tile.y == 0) {
      // Score set
      state.score = tile.id;
    } else {
      if (tile.x < state.minX) state.minX = tile.x;
      if (tile.x > state.maxX) state.maxX = tile.x;
      if (tile.y < state.minY) state.minY = tile.y;
      if (tile.y > state.maxY) state.maxY = tile.y;
      let tilePos = tile.x + ':' + tile.y;
      state.board[tilePos] = tile.id;
      if (tile.id == 3) {
        state.paddlePos = [tile.x, tile.y];
      } else if (tile.id == 4) {
        state.ballPos = [tile.x, tile.y];
      }
    }
    return onDraw(state);
  }

  return execute({
    memory,
    input: parseInput,
    onOutput: parseOutput
  }).then(rs => {
    return state;
  });
}

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: '111111', expected: true, input: '111111' }
      ]
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    let memory = this.props.data[0].split(',').map(v => parseInt(v));
    performance.mark('work-start');
    execute({ memory }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      let drawCommands = chunkArray(this.state.puzzleSolution.output, 3);
      let tiles = {};
      drawCommands.forEach(command => {
        const tilePos = command[0] + ':' + command[1];
        tiles[tilePos] = command[2];
      });
      // Find all blocks
      let count = Object.keys(tiles).reduce((subtotal, key) => {
        if (tiles[key] == 2) {
          subtotal++;
        }
        return subtotal
      }, 0);
      output = (
        <p>{count}</p>
      );
    }

    let memory = this.props.data[0].split(',').map(v => parseInt(v));
    return (
      <div>
        <h1>Part One</h1>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
        <IntcodeConsole memory={memory} />
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
    this.renderGame = this.renderGame.bind(this);
  }

  doWork() {
    const memory = this.props.data[0].split(',').map(v => parseInt(v));
    runGame(memory, this.renderGame).then(rs => {

    });
  }

  renderGame(gameState) {
    let board = [];
    const BOX_SIZE = '12px';
    for (let y = gameState.minY; y <= gameState.maxY; y++) {
      let tileList = [];
      for (let x = gameState.minX; x <= gameState.maxX; x++) {
        let style = {
          flex: '0 0 ' + BOX_SIZE,
          width: BOX_SIZE,
          height: BOX_SIZE,
          color: 'red',
          fontWeight: 'bold',
          textAlign: 'center',
          lineHeight: BOX_SIZE,
          fontSize: BOX_SIZE,
          overflow:'hidden',
          background: 'white'
        };
        const tilePos = x + ':' + y;
        const tileId = gameState.board[tilePos];
        let tileContents = '';
        switch (tileId) {
          case 0:
            // Blank
            break;
          case 1:
            // Wall
            style.background = 'black';
            break;
          case 2:
            // Block
            style.background = 'blue';
            break;
          case 3:
            // Paddle
            style.background = 'red';
            break;
          case 4:
            // Ball
            tileContents = '●';
            break;
          default:
            style.background = 'transparent';
        }
        tileList.push(<div style={style}>{tileContents}</div>);
      }
      board.push(<div style={{ display: 'flex' }}>{tileList}</div>);
    }
    let ballPos;
    if (gameState.ballPos !== false) {
      ballPos = `(${gameState.ballPos[0]}, ${gameState.ballPos[1]})`;
    } else {
      ballPos = 'UNKNOWN';
    }
    board.push(<p><strong>Score:</strong> {gameState.score}, <strong>Ball:</strong> {ballPos}</p>);

    return new Promise((resolve, reject) => {
      if (gameState.score === false) {
        resolve();
        return;
      }
      this.setState(curState => {
        curState.gameBoard = board;
        return curState;
      }, () => {
        setTimeout(e => {
          resolve();
        }, 1);
      });
    });
  }

  render() {
    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doWork()}>Run Game</button>
        </p>
        {this.state.gameBoard}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/13-data.txt');
