import React from 'react';
import ButtonBar from 'ButtonBar';
import { logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute }  from '2019/intcodeProcessor';

function checkCoordinate(program, x, y) {
  return execute({
    memory: program,
    input: [x, y]
  }).then(rs => {
    return {
      x,
      y,
      result: rs.output[0]
    };
  });
}

function checkCoordinate2(x, y) {
  return (x >= y * 0.70945 && x <= y * 0.86609);
}

function doesBoxFit(targetX, targetY, size) {
  for (let y = targetY; y < targetY + size; y++) {
    for (let x = targetX; x < targetX + size; x++) {
      if (!checkCoordinate2(x, y)) {
        return false;
      }
    }
  }
  return true;
}


class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };
  }

  doWork() {
    let memory = this.props.data[0].split(',').map(v => parseInt(v));
    performance.mark('work-start');
    let promiseList = [];
    let MAX_SIZE = 100;
    for (let y = 0; y < MAX_SIZE; y++) {
      let startX = Math.floor(y * 0.7095 - 1);
      if (startX < 0) startX = 0;
      let endX = Math.floor( y * 0.866 + 4);
      if (endX > MAX_SIZE) endX = MAX_SIZE;


      for (let x = startX; x < startX + 5; x++) {
        promiseList.push(checkCoordinate(memory, x, y));
      }
      for (let x = endX - 5; x < endX; x++) {
        promiseList.push(checkCoordinate(memory, x, y));
      }
    }
    Promise.all(promiseList).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      console.log(rs);
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      let coords = {};
      this.state.puzzleSolution.forEach(rs => {
        let key = rs.x + ':' + rs.y;
        coords[key] = rs.result
      });
      let map = [];
      let affectedCount = 0;
      let BOX_SIZE = '6px';
      let MAX_SIZE = 100;
      for (let y = 0; y < MAX_SIZE; y++ ) {
        let tiles = [];
        let beamWidth = 0;
        for (let x = 0; x < MAX_SIZE; x++ ) {
          let style = {
            flex: '0 0 ' + BOX_SIZE,
            width: BOX_SIZE,
            height: BOX_SIZE,
            overflow: 'hidden'
          };
          let calculatedRs = checkCoordinate2(x, y);
          if (calculatedRs) beamWidth++;
          const key = x + ':' + y;
          if (typeof coords[key] == 'undefined') {
            style.background = 'transparent';
          } else if (coords[key] == 1) {
            if (calculatedRs) {
              style.background = 'white';
            } else {
              style.background = 'red';
            }
            affectedCount++;
          } else {
            if (!calculatedRs) {
              style.background = 'black';
            } else {
              style.background = 'blue';
            }
          }
          tiles.push(<div style={style} />);
        }
        map.push(<div style={{ display: 'flex', lineHeight: BOX_SIZE, fontSize: BOX_SIZE }}>{tiles}<div style={{ flex: '0 0 50px' }}>{beamWidth}</div></div>);
      }
      output = (
        <React.Fragment>
          {map}
          <p>{affectedCount}</p>
        </React.Fragment>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        <p>
          <button onClick={e => this.doWork()}>Run Program</button>
        </p>
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    let memory = this.props.data[0].split(',').map(v => parseInt(v));
    performance.mark('work-start');
    let promiseList = [];
    let MAX_SIZE = 1120;
    let minY = MAX_SIZE - 50;
    let maxY = MAX_SIZE;
    for (let y = minY; y < maxY; y++) {
      let startX = Math.floor(y * 0.7095 - 1);
      if (startX < 0) startX = 0;
      let endX = Math.floor( y * 0.866 + 4);
      if (endX > MAX_SIZE) endX = MAX_SIZE;

      for (let x = startX; x < startX + 5; x++) {
        promiseList.push(checkCoordinate(memory, x, y));
      }
      for (let x = endX - 5; x < endX; x++) {
        promiseList.push(checkCoordinate(memory, x, y));
      }
    }
    Promise.all(promiseList).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      console.log(rs);
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let BOX_SIZE = '6px';
    let MAX_SIZE = 1120;
    let minX = Math.floor(MAX_SIZE * 0.67);
    let maxX = MAX_SIZE * 0.9;
    let minY = MAX_SIZE - 50;
    let maxY = MAX_SIZE;

    let output;
    if (this.state.puzzleSolution !== false) {
      let coords = {};
      this.state.puzzleSolution.forEach(rs => {
        let key = rs.x + ':' + rs.y;
        coords[key] = rs.result
      });

      let map = [];
      for (let y = minY; y < maxY; y++ ) {
        let tiles = [];
        let beamWidth = 0;
        for (let x = minX; x < maxX; x++) {
          let style = {
            flex: '0 0 ' + BOX_SIZE,
            width: BOX_SIZE,
            height: BOX_SIZE,
            overflow: 'hidden'
          };
          let calculatedRs = checkCoordinate2(x, y);
          if (calculatedRs) beamWidth++;
          const key = x + ':' + y;
          if (typeof coords[key] == 'undefined') {
            style.background = 'transparent';
          } else if (coords[key] == 1) {
            if (calculatedRs) {
              style.background = 'white';
            } else {
              style.background = 'red';
            }
          } else {
            if (!calculatedRs) {
              style.background = 'black';
            } else {
              style.background = 'blue';
            }
          }
          tiles.push(<div style={style} />);
        }
        map.push(<div style={{ display: 'flex', lineHeight: BOX_SIZE, fontSize: BOX_SIZE }}>{tiles}
          <div style={{ flex: '0 0 50px' }}>{beamWidth}</div>
        </div>);
      }
      output = map;
    }

    // Find position for Santa
    let santaX, santaY;
    for (let y = minY; y < maxY; y++) {
      for (let x = minX; x < maxX; x++) {
        if (doesBoxFit(x, y, 100)) {
          santaX = x;
          santaY = y;
          x = maxX;
          y = maxY;
        }
      }
    }

    let map = [];
    for (let y = minY; y < maxY; y++ ) {
      let tiles = [];
      let beamWidth = 0;
      for (let x = minX; x < maxX; x++) {
        let style = {
          flex: '0 0 ' + BOX_SIZE,
          width: BOX_SIZE,
          height: BOX_SIZE,
          overflow: 'hidden'
        };
        let calculatedRs = checkCoordinate2(x, y);
        if (calculatedRs) beamWidth++;
        if (x == santaX && y == santaY) {
          style.background = 'red';
        } else if (calculatedRs) {
          style.background = 'white';
        } else {
          style.background = 'black';
        }
        tiles.push(<div style={style} />);
      }
      map.push(<div style={{ display: 'flex', lineHeight: BOX_SIZE, fontSize: BOX_SIZE }}>{tiles}
        <div style={{ flex: '0 0 50px' }}>{beamWidth}</div>
      </div>);
    }



    return (
      <div>
        <h1>Part Two</h1>
        <p>Drawn from ({minX}, {minY}) to ({maxX}, {maxY})</p>
        {output}
        <p>Santa ({santaX}, {santaY}).</p>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {map}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/19-data.txt');
