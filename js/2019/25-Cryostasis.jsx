import React from 'react';
import ButtonBar from 'ButtonBar';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute }  from '2019/intcodeProcessor';

/*
wreath
asterisk
astrolabe
monolith
*/
class RobotInterface extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      memory: JSON.parse(JSON.stringify(props.memory)), // Don't clobber the input
      relativeBase: 0,
      pointer: 0,
      output: [],
      currentCommand: ''
    };

    this.runRobot = this.runRobot.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    this._inputCommand = React.createRef();
    this._outputBox = React.createRef();
  }

  runRobot() {
    let input;
    if (this.state.currentCommand == '') {
      input = [null];
    } else {
      input = this.state.currentCommand.split('').map(char => char.charCodeAt(0));
      input.push(10);
      input.push(null);
    }
    execute({
      memory: this.state.memory,
      relativeBase: this.state.relativeBase,
      pointer: this.state.pointer,
      input
    }).then(rs => {
      this.setState(curState => {
        curState.memory = rs.memory;
        curState.relativeBase = rs.relativeBase;
        curState.pointer = rs.pointer;
        if (rs.output.length > 0) {
          if (this.state.currentCommand != '') {
            curState.output.push(this.state.currentCommand);
            curState.output.push('\xa0');
          }
          let programOutput = rs.output.map(val => String.fromCharCode(val)).join('').split("\n");
          curState.output = curState.output.concat(programOutput);
        }
        curState.currentCommand = '';
        return curState;
      }, () => {
        this._inputCommand.current.focus();
        this._outputBox.current.scrollTop = 999999999;
      });
    });
  }

  handleInputChange(e) {
    let newInput = e.target.value;
    this.setState({ currentCommand: newInput });
  }

  componentDidMount() {
    this._inputCommand.current.addEventListener('keypress', e => {
      if (e.key == 'Enter') {
        this.runRobot();
      }
    });
  }

  render() {
    let programOutput = this.state.output.map((line, index) => {
      return (
        <p key={index}>{line}</p>
      );
    });
    return (
      <div className="intcode-console">
        <div ref={this._outputBox} className="program-output">
          {programOutput}
        </div>
        <p>
          <input ref={this._inputCommand} onChange={this.handleInputChange} value={this.state.currentCommand} />
        </p>
        <p>
          <button onClick={e => this.runRobot()}>Run</button>
        </p>
      </div>
    );
  }
}

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: '111111', expected: true, input: '111111' }
      ]
    };
  }

  render() {
    const memory = this.props.data[0].split(',').map(i => parseInt(i));

    return (
      <div>
        <h1>Part One</h1>
        <RobotInterface memory={memory} />
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: '111111', expected: true, input: '111111' }
      ]
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    performance.mark('work-start');
    const data = this.props.data;
    this._worker.send({
      cmd: 'solve-puzzle',
      arg: data
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/25-data.txt');
