import React from 'react';
import ButtonBar from 'ButtonBar';
import { logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute }  from '2019/intcodeProcessor';

function runProgram(memory, onDraw) {
  let state = {
    outputLog: []
  };
  function parseOutput(data) {
    state.outputLog.push(data);
    return onDraw(state);
  }
  return execute({
    memory
  });
}

function plotPath(map, startingPos) {
  let currentPos = [startingPos[0], startingPos[1]];
  let currentDirection = 0;
  let instructions = [];
  let cycles = 0;
  while(cycles < 9999) {
    // Check sides
    let left = false;
    let right = false;
    switch(currentDirection) {
      case 0:
        // Up
        if (currentPos[0] > 0) {
          left = map[currentPos[1]][currentPos[0] - 1];
        }
        if (currentPos[0] < map[currentPos[1]].length - 1) {
          right = map[currentPos[1]][currentPos[0] + 1];
        }
        break;
      case 1:
        // Right
        if (currentPos[1] > 0) {
          left = map[currentPos[1] - 1][currentPos[0]];
        }
        if (currentPos[1] < map.length - 1) {
          right = map[currentPos[1] + 1][currentPos[0]];
        }
        break;
      case 2:
        // Down
        if (currentPos[0] < map[currentPos[1]].length - 1) {
          left = map[currentPos[1]][currentPos[0] + 1];
        }
        if (currentPos[0] > 0) {
          right = map[currentPos[1]][currentPos[0] - 1];
        }
        break;
      case 3:
        // Left
        if (currentPos[1] < map.length - 1) {
          left = map[currentPos[1] + 1][currentPos[0]];
        }
        if (currentPos[1] > 0) {
          right = map[currentPos[1] - 1][currentPos[0]];
        }
        break;
    }
    if ((left === false || left == '.') && (right === false || right == '.')) {
      // Done!
      break;
    }
    if (left == '#') {
      // Turn to the left and advance
      instructions.push('L');
      currentDirection--;
      if (currentDirection < 0) currentDirection += 4;
    } else {
      // Turn to the right and advance
      instructions.push('R');
      currentDirection++;
      if (currentDirection > 3) currentDirection -= 4;
    }
    let distance = false;
    switch(currentDirection) {
      case 0: {
        // Up
        let y;
        for (y = currentPos[1]; y >= 0; y--) {
          if (map[y][currentPos[0]] != '#') {
            break;
          }
        }
        distance = currentPos[1] - y - 1;
        instructions.push(distance);
        currentPos[1] = y + 1;
        break;
      }
      case 1: {
        // Right
        let x;
        for (x = currentPos[0]; x < map[currentPos[1]].length; x++) {
          if (map[currentPos[1]][x] != '#' && map[currentPos[1]][x] != '^') {
            break;
          }
        }
        distance = x - currentPos[0] - 1;
        instructions.push(distance);
        currentPos[0] = x - 1;
        break;
      }
      case 2: {
        // Down
        let y;
        for (y = currentPos[1]; y < map.length; y++) {
          if (map[y][currentPos[0]] != '#') {
            break;
          }
        }
        distance = y - currentPos[1] - 1;
        instructions.push(distance);
        currentPos[1] = y - 1;
        break;
      }
      case 3: {
        // Left
        let x;
        for (x = currentPos[0]; x >= 0; x--) {
          if (map[currentPos[1]][x] != '#') {
            break;
          }
        }
        distance = currentPos[0] - x - 1;
        instructions.push(distance);
        currentPos[0] = x + 1;
        break;
      }
    }
    if (distance <= 0) {
      debugger;
      break;
    }
    cycles++;
  }
  return instructions;
}

const map = [
  "..............###########................",
  "..............#.........#................",
  "..............#.........#................",
  "..............#.........#................",
  "..............#.........#................",
  "..............#.........#................",
  "..............#############..............",
  "........................#.#..............",
  "........................#.#..............",
  "........................#.#..............",
  "............^############.#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#..............",
  "..........................#####..........",
  "..............................#..........",
  "..............................#...#######",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "..............................#...#.....#",
  "....#######...................#...#.....#",
  "..........#...................#...#.....#",
  "..........#.............#####.###########",
  "..........#.............#...#.....#......",
  "..........#...........#############......",
  "..........#...........#.#...#............",
  "..........#...........#.#...#............",
  "..........#...........#.#...#............",
  "..........#...........#############......",
  "..........#.............#...#.....#......",
  "..........#.............#...#.....#......",
  "..........#.............#...#.....#......",
  "..........#############.#...#.....#......",
  "......................#.#...#.....#......",
  "............#############...#############",
  "............#.........#...........#.....#",
  "............#.........#...........#.....#",
  "............#.........#...........#.....#",
  "..........#############...........#.....#",
  "..........#.#.....................#.....#",
  "#############.....................#######",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "#.........#..............................",
  "###########.............................."
];

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
    this.renderResult = this.renderResult.bind(this);
  }

  renderResult(programState) {
    console.log(programState);
  }

  doWork() {
    let memory = this.props.data[0].split(',').map(v => parseInt(v));
    performance.mark('work-start');
    runProgram(memory, this.renderResult).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {

    let output;
    if (this.state.puzzleSolution !== false) {
      let rawOutput = this.state.puzzleSolution.output;
      let map = '';
      rawOutput.forEach(char => {
        map += String.fromCharCode(char);
      });

      // Find intersections
      let lines = map.split("\n");
      console.log(JSON.stringify(lines));
      let intersections = [];
      for (let y = 1; y < lines.length - 1; y++) {
        for (let x = 1; x < lines[y].length - 1; x++) {
          if (lines[y][x] == '#' &&
            lines[y - 1][x] == '#' && lines[y + 1][x] == '#' &&
            lines[y][x + 1] == '#' && lines[y][x - 1] == '#') {
              intersections.push({ x, y });
          }
        }
      }
      let alignmentSum = intersections.reduce((subtotal, int) => {
        return subtotal + int.x * int.y;
      }, 0);

      output = (
        <React.Fragment>
          <p style={{ lineHeight: '1em', whiteSpace: 'pre', fontFamily: 'monospace' }}>{map}</p>
          <p>{alignmentSum}</p>
        </React.Fragment>
      );
    }
    let memory = this.props.data[0].split(',').map(v => parseInt(v));

    return (
      <div>
        <h1>Part One</h1>
        <p>
          <button onClick={e => this.doWork()}>Run Program</button>
        </p>
        {output}
        <IntcodeConsole memory={memory} />
      </div>
    );
  }
}

const funcA = ['R', 12, 'L', 10, 'L', 10];
const funcB = ['L', 6, 'L', 12, 'R', 12, 'L', 4];
const funcC = ['L', 12, 'R', 12, 'L', 6];

const A = <span style={{ background: 'rgba(255, 0, 0, 0.2)' }}>{funcA.join(', ')}</span>;
const B = <span style={{ background: 'rgba(0, 255, 0, 0.2)' }}>{funcB.join(', ')}</span>;
const C = <span style={{ background: 'rgba(0, 0, 255, 0.2)' }}>{funcC.join(', ')}</span>;

const functionOrder = ['A', 'B', 'A', 'B', 'C', 'B', 'C', 'A', 'C', 'C'];

function arrToAscii(arr) {
  let out = [];
  let str = arr.join(',');
  for (let i = 0; i < str.length; i++) {
    out.push(str.charCodeAt(i));
  }
  return out;
}

const input = arrToAscii(functionOrder).concat(
  [10],
  arrToAscii(funcA),
  [10],
  arrToAscii(funcB),
  [10],
  arrToAscii(funcC),
  [10],
  arrToAscii(['n']),
  [10]
);


class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    performance.mark('work-start');

    let memory =  this.props.data[0].split(',').map(v => parseInt(v));
    memory[0] = 2; // ACTIVATE!!!

    execute({
      memory,
      input
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let path = plotPath(map, [12, 10]);
    let preview = (
      <div>
        <h2>Original:</h2>
        <p style={{ lineHeight: '1em', fontFamily: 'monospace' }}>
          {path.join(', ')}
        </p>
        <h2>Assembled:</h2>
        <p style={{ lineHeight: '1em', fontFamily: 'monospace' }}>
          {A}, {B}, {A}, {B}, {C}, {B}, {C}, {A}, {C}, {C}
        </p>
        <p style={{ lineHeight: '1em', fontFamily: 'monospace' }}>
          {input.join(', ')}
        </p>
      </div>
    );

    let output;
    if (this.state.puzzleSolution !== false) {
      let score = this.state.puzzleSolution.output.slice(-1);
      output = (
        <p>{score}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p style={{ lineHeight: '1em', whiteSpace: 'pre', fontFamily: 'monospace' }}>{map.join("\n")}</p>
        {preview}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/17-data.txt');
