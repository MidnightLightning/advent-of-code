import React from 'react';
import ButtonBar from 'ButtonBar';
import { logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute}  from '2019/intcodeProcessor';

function getInput({ robotPos, panels }) {
  let panelId = robotPos[0] + ':' + robotPos[1];
  let color = 0;
  if (typeof panels[panelId] !== 'undefined') {
    color = panels[panelId];
  }
  //console.log('Input', count, color);
  return color;
}

function getOutput(data, curState) {
  curState.outputLog.push(data);
  if (curState.outputLog.length < 2) return curState;

  const color = curState.outputLog[0];
  const turn = curState.outputLog[1];
  curState.outputLog = [];

  curState.painted.push({
    panel: [curState.robotPos[0], curState.robotPos[1]],
    color: color
  });
  curState.panels[curState.robotPos[0] + ':' + curState.robotPos[1]] = color;

  if (turn == 0) {
    curState.robotFacing--;
  } else {
    curState.robotFacing++;
  }
  if (curState.robotFacing > 3) {
    curState.robotFacing -= 4;
  }
  if (curState.robotFacing < 0) {
    curState.robotFacing += 4;
  }
  switch (curState.robotFacing) {
    case 0:
      curState.robotPos[1]--;
      break;
    case 1:
      curState.robotPos[0]++;
      break;
    case 2:
      curState.robotPos[1]++;
      break;
    case 3:
      curState.robotPos[0]--;
      break;
  }
  //console.log(`Robot at (${curState.robotPos[0]}, ${curState.robotPos[1]}), painted ${color}, and then turned ${turn} (${curState.robotFacing})`, curState.panels);
  return curState;
}

function runRobot(memory, startingSquare = 0) {
  let state = {
    robotPos: [0, 0],
    robotFacing: 0,
    painted: [],
    panels: {},
    outputLog: []
  };
  if (startingSquare) {
    state.panels['0:0'] = startingSquare;
  }

  function parseInput(count) {
    return getInput(state);
  }
  function parseOutput(data) {
    state = getOutput(data, state);
  }

  return execute({
    memory,
    input: parseInput,
    onOutput: parseOutput
  }).then(rs => {
    return state;
  });
}

function renderMap({ robotPos, robotFacing, painted, panels}) {
  let minX = -10,
    maxX = 10,
    minY = -10,
    maxY = 10;
  // Determine plot bounds
  painted.forEach(p => {
    if (p.panel[0] < minX) minX = p.panel[0];
    if (p.panel[0] > maxX) maxX = p.panel[0];
    if (p.panel[1] < minY) minY = p.panel[1];
    if (p.panel[1] > maxY) maxY = p.panel[1];
  });
  if (robotPos[0] < minX) minX = robotPos[0];
  if (robotPos[0] > maxX) maxX = robotPos[0];
  if (robotPos[1] < minY) minX = robotPos[1];
  if (robotPos[1] > maxY) maxY = robotPos[1];
  let map = [];
  const BOX_SIZE = '12px';
  for (let y = minY; y <= maxY; y++) {
    let panelList = [];
    for (let x = minX; x <= maxX; x++) {
      let style = { flex: '0 0 ' + BOX_SIZE, width: BOX_SIZE, height: BOX_SIZE, color: 'red', fontWeight: 'bold', textAlign: 'center', lineHeight: BOX_SIZE, fontSize: BOX_SIZE, overflow:'hidden' };
      let panelId = x + ':' + y;
      let robot = '';
      if (robotPos[0] == x && robotPos[1] == y) {
        switch(robotFacing) {
          case 0:
            robot = '▲';
            break;
          case 1:
            robot = '▶';
            break;
          case 2:
            robot = '▼';
            break;
          case 3:
            robot = '◀';
            break;
        }
      }
      if (panels[panelId] == 1) {
        style.background = 'white';
      } else {
        style.background = 'black';
      }
      panelList.push(<div style={style}>{robot}</div>);
    }
    map.push(<div style={{ display:'flex' }}>{panelList}</div>);
  }
  map.push(
    <p><strong>Number of paint actions:</strong> {painted.length},&nbsp;
      <strong>Painted Panels:</strong> {Object.keys(panels).length}</p>
  );
  return map;
}

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      puzzleSolution: false,
      robotPos: [0, 0],
      robotFacing: 0,
      painted: [],
      panels: { '0:0': 1 },
      outputLog: []
    };

    this.parseInput = this.parseInput.bind(this);
    this.parseOutput = this.parseOutput.bind(this);
  }

  doWork() {
    performance.mark('work-start');
    const memory = this.props.data[0].split(',').map(i => parseInt(i));
    runRobot(memory).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      console.log(rs);
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  parseInput(count) {
    return getInput(this.state);
  }

  parseOutput(data) {
    this.setState(curState => {
      return getOutput(data, curState);
    });
  }

  render() {
    const memory = this.props.data[0].split(',').map(i => parseInt(i));

    let output;
    if (this.state.puzzleSolution !== false) {
      output = renderMap(this.state.puzzleSolution);
    }

    let map = renderMap(this.state);

    return (
      <div>
        <h1>Part One</h1>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
        <IntcodeConsole
          memory={memory}
          onInput={this.parseInput}
          onOutput={this.parseOutput}
          showOutput={false}
        >
          {map}
        </IntcodeConsole>
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    performance.mark('work-start');
    const memory = this.props.data[0].split(',').map(i => parseInt(i));
    runRobot(memory, 1).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      console.log(rs);
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {

    let output;
    if (this.state.puzzleSolution !== false) {
      output = renderMap(this.state.puzzleSolution);
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/11-data.txt');
