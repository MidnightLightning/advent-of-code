import React from 'react';
import ButtonBar from 'ButtonBar';
import { WorkerQueue } from 'workerUtils';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: 'Sample map', expected: 42, input: ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"] }
      ]
    };
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/06-worker.js'));
    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'sum-orbits', orbitMap: test.input });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });

    const data = this.props.data;
    this._worker.send({
      cmd: 'sum-orbits',
      orbitMap: data
    }).then(rs => {
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        {testResults}
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: 'Sample map', expected: 4, input: ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"] }
      ]
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/06-worker.js'));
    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'transfers-needed', orbitMap: test.input });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doWork() {
    performance.mark('work-start');
    const data = this.props.data;
    this._worker.send({
      cmd: 'transfers-needed',
      orbitMap: data
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/06-data.txt');
