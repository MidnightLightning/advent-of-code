import React from 'react';
import ButtonBar from 'ButtonBar';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute}  from '2019/intcodeProcessor';

const quine = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };
  }

  componentDidMount() {
    let testPromises = [];
    testPromises.push(execute({ memory: quine }).then(rs => rs.output));
    testPromises.push(execute({ memory: [1102,34915192,34915192,7,4,7,99,0] }).then(rs => (''+rs.output[0]).length));
    testPromises.push(execute({ memory: [104,1125899906842624,99] }).then(rs => rs.output[0]));

    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs;
        return curState;
      });
    });
  }

  render() {
    let unitTests = [
      { label: 'Quine', expected: quine },
      { label: 'BigNum', expected: 16 },
      { label: 'BigNum2', expected: 1125899906842624 }
    ];
    let memory = this.props.data[0].split(',').map(value => parseInt(value));

    return (
      <div>
        <h1>Part One</h1>
        {showAsyncTestResults(unitTests, this.state.testResults)}
        <IntcodeConsole memory={memory} />
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork() {
    performance.mark('work-start');
    let memory = this.props.data[0].split(',').map(value => parseInt(value));
    let rs = execute({
      memory: memory,
      input: [2]
    });
    logPerformanceEnd('work-start', 'work-end');
    this.setState(curState => {
      curState.puzzleSolution = rs.output[0];
      return curState;
    });
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/09-data.txt');
