import React from 'react';
import IntcodeMemory from '2019/IntcodeMemory';
import { step, getInstruction } from '2019/intcodeProcessor';

function chunkArray(arr, chunkLength) {
  let tmp = [];
  for (let i = 0; i < arr.length; i+=chunkLength) {
    tmp.push(arr.slice(i, i + chunkLength));
  }
  return tmp;
}

class IntcodeConsole extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pointer: (typeof props.pointer !== 'undefined') ? props.pointer : 0,
      cycles: 0,
      relativeBase: 0,
      inputPointer: 0,
      memory: JSON.parse(JSON.stringify(props.memory)),
      output: [],
      isRunning: false,
      runSpeed: 200
    };

    this.doReset = this.doReset.bind(this);
    this.doStep = this.doStep.bind(this);
    this.doRun = this.doRun.bind(this);
    this.doStop = this.doStop.bind(this);
    this.doFaster = this.doFaster.bind(this);
    this.doSlower = this.doSlower.bind(this);
    this.runCycle = this.runCycle.bind(this);
  }

  doReset() {
    this.setState(curState => {
      curState.cycles = 0;
      curState.pointer = (typeof this.props.pointer != 'undefined') ? this.props.pointer : 0;
      curState.memory = JSON.parse(JSON.stringify(this.props.memory));
      curState.relativeBase = 0;
      curState.output = [];
      curState.isRunning = false;
      curState.runSpeed = 200;
      return curState;
    });
  }

  doStep(callback) {
    let opts = {
      memory: this.state.memory,
      pointer: this.state.pointer,
      relativeBase: this.state.relativeBase
    };
    if (typeof this.props.onInput == 'function') {
      // Automated input
      opts.input = this.props.onInput(this.state.inputPointer);
    }
    let { pointer, memory, output, lastInstruction, relativeBase, doContinue } = step(opts);
    if (output !== false && typeof this.props.onOutput == 'function') {
      this.props.onOutput(output);
    }

    let haltHandler = this.props.onHalt;
    function workDone() {
      if (typeof callback == 'function') {
        callback(lastInstruction);
      }
      if (doContinue === false && typeof haltHandler == 'function') {
        haltHandler(this.state.output);
      }
    }

    if (doContinue === false) {
      workDone();
      return;
    }
    this.setState(curState => {
      if (doContinue !== false) {
        // Only advance pointer if continuing
        curState.pointer = pointer;
        curState.cycles++;
      }
      curState.memory = memory;
      curState.relativeBase = relativeBase;
      if (output !== false) {
        curState.output.push(output);
      }
      if (lastInstruction.label == 'INPUT') {
        curState.inputPointer++;
      }
      return curState;
    }, workDone);
  }

  runCycle() {
    if (this.state.isRunning == false) return;
    this.doStep((lastInstruction) => {
      let automatedInput = (typeof this.props.onInput == 'function');
      if ((lastInstruction.label == 'INPUT' && !automatedInput) ||
        lastInstruction.label == 'HALT' ||
        lastInstruction.label == 'UNKNOWN'
      ) {
        // Halt the process
        this.setState(curState => {
          curState.isRunning = false;
          return curState;
        });
        return;
      }

      // Schedule next run
      window.setTimeout(() => {
        window.requestAnimationFrame(this.runCycle);
      }, this.state.runSpeed)
    });
  }

  doRun() {
    this.setState(curState => {
      curState.isRunning = true;
      return curState;
    }, this.runCycle);
  }

  doFaster() {
    this.setState(curState => {
      curState.runSpeed = 2;
      return curState;
    });
  }

  doSlower() {
    this.setState(curState => {
      curState.runSpeed = 200;
      return curState;
    });
  }

  doStop() {
    this.setState(curState => {
      curState.isRunning = false;
      return curState;
    });
  }

  render() {
    let currentInstruction = getInstruction(this.state.memory, this.state.pointer, this.state.relativeBase);

    let paramVisuals = currentInstruction.parameters.map(param => {
      if (param.mode === 0) {
        return `[${param.rawValue}]`;
      } else if (param.mode === 1) {
        return '' + param.value;
      } else if (param.mode === 2) {
        return `{${param.rawValue}}`;
      }
    });

    let programOutput;
    if (this.state.output.length > 0 && this.props.showOutput !== false) {
      programOutput = (
        <div className="program-output">
          {
            this.state.output.map((line, index) => {
              return (
                <p key={index}>{line}</p>
              );
            })
          }
        </div>
      );
    }

    let runningNotice = ' ';
    let runBtn;
    if (this.state.isRunning) {
      runBtn = (
        <React.Fragment >
          <button onClick={e => this.doStop()}>Stop</button>
          <button onClick={e => this.doSlower()}>&gt;</button>
          <button onClick={e => this.doFaster()}>&gt;&gt;</button>
        </React.Fragment>
      );
      runningNotice = 'Running...';
    } else {
      runBtn = (
        <button onClick={e => this.doRun()}>Run</button>
      );
    }
    runningNotice = (
      <span style={{ fontStyle: 'italic', color: '#888' }}>{runningNotice}</span>
    );

    return (
      <div className="intcode-console">
        <IntcodeMemory
          memory={this.state.memory}
          pointer={this.state.pointer}
          relativeBase={this.state.relativeBase}
        />
        <p className="action-details">
          <strong>Cycles:</strong> {this.state.cycles},&nbsp;
          <strong>Pointer:</strong> {this.state.pointer},&nbsp;
          <strong>Relative Base: </strong> {this.state.relativeBase},&nbsp;
          <strong>Current Opcode:</strong> {currentInstruction.label},&nbsp;
          <strong>Opcode Params:</strong> {paramVisuals.join(', ')}
        </p>
        <p className="action-buttons">
          <button onClick={e => this.doReset()}>Reset</button>
          <button onClick={e => this.doStep()}>Step</button>
          {runBtn}
          {runningNotice}
        </p>
        {this.props.children}
        {programOutput}
      </div>
    );
  }
}

export default IntcodeConsole;
