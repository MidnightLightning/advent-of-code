import React from 'react';
import ButtonBar from 'ButtonBar';
import { WorkerQueue } from 'workerUtils';
import { logPerformanceEnd } from 'puzzleUtils';

const INPUT = [
  [15, -2, -6],
  [-5, -4, -11],
  [0, -6, 0],
  [5, 9, 6]
];

const TEST = [
  [-1, 0, 2],
  [2, -10, -7],
  [4, -8, 8],
  [3, 5, -1]
];

function tick(moons) {
  let gravities = moons.map((moon, moonIndex) => {
    let gravity = [0, 0, 0];
    for (let i = 0; i < moons.length; i++) {
      if (i !== moonIndex) {
        // Compare to other moon
        if (moons[i].pos[0] < moon.pos[0]) {
          gravity[0]--;
        } else if (moons[i].pos[0] > moon.pos[0]) {
          gravity[0]++;
        }
        if (moons[i].pos[1] < moon.pos[1]) {
          gravity[1]--;
        } else if (moons[i].pos[1] > moon.pos[1]) {
          gravity[1]++;
        }
        if (moons[i].pos[2] < moon.pos[2]) {
          gravity[2]--;
        } else if (moons[i].pos[2] > moon.pos[2]) {
          gravity[2]++;
        }
      }
    }
    return gravity;
  });

  return moons.map((moon, index) => {
    const gravity = gravities[index];
    let newMoon = {
      vel: [
        moon.vel[0] + gravity[0],
        moon.vel[1] + gravity[1],
        moon.vel[2] + gravity[2]
      ]
    };
    newMoon.pos = [
      moon.pos[0] + newMoon.vel[0],
      moon.pos[1] + newMoon.vel[1],
      moon.pos[2] + newMoon.vel[2]
    ];
    return newMoon;
  });
}

function simulate(moonInitialPositions, cycles) {
  let moons = [];
  moonInitialPositions.forEach(moonPos => {
    moons.push({
      pos: [moonPos[0], moonPos[1], moonPos[2]],
      vel: [0, 0, 0]
    });
  });
  for (let i = 0; i < cycles; i++) {
    moons = tick(moons);
    //console.log(i+1, moons);
  }
  return moons;
}

function totalEnergy(moons) {
  return moons.reduce((subtotal, moon) => {
    let potential = Math.abs(moon.pos[0]) + Math.abs(moon.pos[1]) + Math.abs(moon.pos[2]);
    let kinetic = Math.abs(moon.vel[0]) + Math.abs(moon.vel[1]) + Math.abs(moon.vel[2]);
    return subtotal + potential * kinetic;
  }, 0)
}

function showVector(vector) {
  return (
    <span>({vector[0]}, {vector[1]}, {vector[2]})</span>
  );
}

function showSystem(moons) {
  return (
    <div>
      { moons.map(moon => {
        return (
          <p>Position {showVector(moon.pos)}, Velocity {showVector(moon.vel)}</p>
        );
      }) }
      <p>Energy: {totalEnergy(moons)}</p>
    </div>
  );
}

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };
  }

  componentDidMount() {
    // Do tests
    let rs = simulate(TEST, 10);
    let puzzle = simulate(INPUT, 1000);
    this.setState(curState => {
      curState.testResults = rs;
      curState.puzzleSolution = puzzle;
      return curState;
    });
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      output = showSystem(this.state.puzzleSolution);
    }

    let test;
    if (this.state.testResults !== false) {
      test = showSystem(this.state.testResults);
    }

    return (
      <div>
        <h1>Part One</h1>
        {test}
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doTest = this.doTest.bind(this);
    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/12-worker.js'));
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doTest() {
    performance.mark('work-start');
    this._worker.send({
      cmd: 'find-collision',
      moons: TEST
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.testResults = rs.result;
        return curState;
      });
    });
  }

  doWork() {
    performance.mark('work-start');
    this._worker.send({
      cmd: 'find-collision',
      moons: INPUT
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.testResults = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults;
    if (this.state.testResults !== false) {
      testResults = (
        <p>{this.state.testResults}</p>
      );
    }

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doTest()}>Run Tests</button>
        </p>
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default PuzzleView;
