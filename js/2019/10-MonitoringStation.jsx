import React from 'react';
import ButtonBar from 'ButtonBar';
import { WorkerQueue } from 'workerUtils';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';
import UnitTests from 'UnitTests';

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: 'Small Sample', expected: [3, 4], input: [
          '.#..#',
          '.....',
          '#####',
          '....#',
          '...##'
        ]},
        { label: 'Test 1', expected: [5, 8], input: [
          '......#.#.',
          '#..#.#....',
          '..#######.',
          '.#.#.###..',
          '.#..#.....',
          '..#....#.#',
          '#..#....#.',
          '.##.#..###',
          '##...#..#.',
          '.#....####'
        ]},
        { label: 'Test 2', expected: [1, 2], input: [
          '#.#...#.#.',
          '.###....#.',
          '.#....#...',
          '##.#.#.#.#',
          '....#.#.#.',
          '.##..###.#',
          '..#...##..',
          '..##....##',
          '......#...',
          '.####.###.'
        ]},
        { label: 'Test 3', expected: [6, 3], input: [
          '.#..#..###',
          '####.###.#',
          '....###.#.',
          '..###.##.#',
          '##.##.#.#.',
          '....###..#',
          '..#.#..#.#',
          '#..#.#.###',
          '.##...##.#',
          '.....#.#..'
        ]},
        { label: 'Test 3', expected: [11, 13], input: [
          '.#..##.###...#######',
          '##.############..##.',
          '.#.######.########.#',
          '.###.#######.####.#.',
          '#####.##.#.##.###.##',
          '..#####..#.#########',
          '####################',
          '#.####....###.#.#.##',
          '##.#################',
          '#####.##.###..####..',
          '..######..##.#######',
          '####.##.####...##..#',
          '.#####..#.######.###',
          '##...#.##########...',
          '#.##########.#######',
          '.####.#.###.###.#.##',
          '....##.##.###..#####',
          '.#.#.###########.###',
          '#.#.#.#####.####.###',
          '###.##.####.##.#..##'
        ]}
      ]
    };
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/10-worker.js'));
    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'find-best', map: test.input });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result.coordinate);
        return curState;
      });
    });

    const data = this.props.data;
    this._worker.send({
      cmd: 'find-best',
      map: data
    }).then(rs => {
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      const coordinate = this.state.puzzleSolution.coordinate;
      output = (
        <p>{coordinate.join(', ')}: {this.state.puzzleSolution.visible}</p>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        {testResults}
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        {
          label: 'Small Test',
          expected: [
           '8:1', '9:0', '9:1', '10:0', '9:2', '11:1', '12:1', '11:2'
          ],
          map: [
            '.#....#####...#..',
            '##...##.#####..##',
            '##...#...#.#####.',
            '..#.....#...###..',
            '..#.#.....#....##'
          ],
          station: [8, 3]
        },
        {
          label: 'Bigger test',
          expected: [ '11:12', '12:1', '12:2' ],
          map: [
            '.#..##.###...#######',
            '##.############..##.',
            '.#.######.########.#',
            '.###.#######.####.#.',
            '#####.##.#.##.###.##',
            '..#####..#.#########',
            '####################',
            '#.####....###.#.#.##',
            '##.#################',
            '#####.##.###..####..',
            '..######..##.#######',
            '####.##.####...##..#',
            '.#####..#.######.###',
            '##...#.##########...',
            '#.##########.#######',
            '.####.#.###.###.#.##',
            '....##.##.###..#####',
            '.#.#.###########.###',
            '#.#.#.#####.####.###',
            '###.##.####.##.#..##'
          ],
          station: [11, 13]
        }
      ]
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/10-worker.js'));
    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'vaporized-order', map: test.map, x: test.station[0], y: test.station[1] });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => {
          return row.result
        });
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doWork() {
    performance.mark('work-start');
    const data = this.props.data;
    this._worker.send({
      cmd: 'vaporized-order',
      map: data,
      x: 28,
      y: 29
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults;
    let testDump;
    if (this.state.testResults !== false) {
      let unitTests = [];
      this.state.testCases.forEach((test, index) => {
        test.expected.forEach((expected, order) => {
          unitTests.push({
            label: `${test.label}: ${order}`,
            expected: expected,
            actual: this.state.testResults[index][order].join(':')
          });
        });
      });
      testResults = (
        <UnitTests tests={unitTests} />
      );

      testDump = (
        <textarea style={{ width: '100%', height: 200 }}>
          {this.state.testResults[1].map((row, index) => {
            return `${index + 1}: (${row[0]}, ${row[1]})`;
          }).join("\n")}
        </textarea>
      );
    }

    let output;
    if (this.state.puzzleSolution !== false) {
      output = this.state.puzzleSolution.map((row, index) => {
        return (
          <p>{index+1}: ({row[0]}, {row[1]})</p>
        );
      });
    }

    return (
      <div>
        <h1>Part Two</h1>
        {testDump}
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/10-data.txt');
