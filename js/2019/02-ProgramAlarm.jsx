import React from 'react';
import { showAsyncTestResults, withData } from 'puzzleUtils';
import ButtonBar from 'ButtonBar';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute }  from '2019/intcodeProcessor';

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { program: [1,9,10,3,2,3,11,0,99,30,40,50], expected: [3500,9,10,70, 2,3,11,0, 99, 30,40,50]},
        { program: [1,0,0,0,99], expected: [2,0,0,0,99] },
        { program: [2,3,0,3,99], expected: [2,3,0,6,99] },
        { program: [2,4,4,5,99,0], expected: [2,4,4,5,99,9801] },
        { program: [1,1,1,4,99,5,6,0,99], expected: [30,1,1,4,2,5,6,0,99] },
      ]
    };
  }

  componentDidMount() {
    let testPromises = this.state.testCases.map(test => {
      return execute({
        memory: test.program
      }).then(rs => {
        return rs.memory;
      });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs;
        return curState;
      });
    });

    let memory = this.props.data[0].split(',').map(value => parseInt(value));
    memory[1] = 12;
    memory[2] = 2;
    execute({ memory }).then(rs => {
      this.setState(curState => {
        curState.puzzleSolution = rs;
      });
    });
  }

  render() {
    let memory = this.props.data[0].split(',').map(value => parseInt(value));

    let unitTests = this.state.testCases.map(test => {
      return { label: test.program.join(','), expected: test.expected}
    });

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>Output: {this.state.puzzleSolution.memory[0]}</p>
      )
    }

    return (
      <div>
        <h1>Part One</h1>
        {showAsyncTestResults(unitTests, this.state.testResults)}
        <IntcodeConsole memory={memory} />
        {output}
      </div>
    );
  }
}

function findGoal(program, goal) {
  for (let noun = 0; noun <= 99; noun++) {
    for (let verb = 0; verb <= 99; verb++) {
      program[1] = noun;
      program[2] = verb;
      let output = execute({ memory: program }).memory;
      if (output[0] === goal) {
        return [noun, verb];
      }
    }
  }
  return false;
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      result: false
    };

    this.doWork = this.doWork.bind(this);
  }

  doWork(program, goal) {
    let rs = findGoal(program, goal);
    this.setState(curState => {
      curState.result = rs;
      return curState;
    });
  }

  render() {
    let original = this.props.data[0].split(',').map(value => parseInt(value));
    const goal = 19690720;

    let noun, verb, output;
    if (this.state.result !== false) {
      [noun, verb] = this.state.result;
      output = (
        <p>{100 * noun + verb}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doWork(original, goal)}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/02-data.txt');
