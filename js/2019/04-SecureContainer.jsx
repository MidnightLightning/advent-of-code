import React from 'react';
import ButtonBar from 'ButtonBar';
import { WorkerQueue } from 'workerUtils';
import { showAsyncTestResults, logPerformanceEnd } from 'puzzleUtils';

const PUZZLE_MIN = 235741;
const PUZZLE_MAX = 706948;

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: '112233', expected: true, input: '112233' },
        { label: '223450', expected: false, input: '223450' },
        { label: '123789', expected: false, input: '123789' }
      ],

    };
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/04-worker.js'));

    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'is-valid2', pwd: test.input });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });

    performance.mark('work-start');
    this._worker.send({
      cmd: 'check-range',
      min: PUZZLE_MIN,
      max: PUZZLE_MAX,
      threads: 4
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution.length}</p>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        {testResults}
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      testCases: [
        { label: '112233', expected: true, input: '112233' },
        { label: '123444', expected: false, input: '123444' },
        { label: '111122', expected: true, input: '111122' }
      ],
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/04-worker.js'));

    let testPromises = this.state.testCases.map(test => {
      return this._worker.send({ cmd: 'is-valid2', pwd: test.input });
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doWork() {
    performance.mark('work-start');
    this._worker.send({
      cmd: 'check-range2',
      min: PUZZLE_MIN,
      max: PUZZLE_MAX,
      threads: 4
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults = showAsyncTestResults(this.state.testCases, this.state.testResults);

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution.length}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent />
      </section>
    );
  }
}

export default PuzzleView;
