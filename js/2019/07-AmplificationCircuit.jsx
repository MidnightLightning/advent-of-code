import React from 'react';
import ButtonBar from 'ButtonBar';
import { logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import IntcodeMemory from '2019/IntcodeMemory';
import { execute }  from '2019/intcodeProcessor';
import UnitTests from 'UnitTests';

class Amplifier {
  constructor({ phaseSetting, program }) {
    this.state = {
      inputQueue: [phaseSetting],
      memory: program,
      pointer: 0,
      relativeBase: 0,
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.doRun = this.doRun.bind(this);
  }

  inputHandler(destAddress) {
    if (this.state.inputQueue.length > 0) {
      return this.state.inputQueue.shift();
    } else {
      return null; // Pause and wait for more input
    }
  }

  doRun(inputParam) {
    if (typeof inputParam !== 'undefined' && inputParam !== null) {
      this.state.inputQueue.push(inputParam);
    }
    return execute({
      memory: this.state.memory,
      input: this.inputHandler,
      pointer: this.state.pointer,
      relativeBase: this.state.relativeBase
    }).then(rs => {
      this.state.memory = rs.memory;
      this.state.pointer = rs.pointer;
      this.state.relativeBase  = rs.relativeBase;
      return rs;
    });
  }
}

class AmplifierConsole extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      amps: [
        new Amplifier({
          phaseSetting: props.phaseSetting[0],
          program: props.program
        }),
        new Amplifier({
          phaseSetting: props.phaseSetting[1],
          program: props.program
        }),
        new Amplifier({
          phaseSetting: props.phaseSetting[2],
          program: props.program
        }),
        new Amplifier({
          phaseSetting: props.phaseSetting[3],
          program: props.program
        }),
        new Amplifier({
          phaseSetting: props.phaseSetting[4],
          program: props.program
        })
      ],
      cycles: 0,
      signal: 0,
      currentAmp: 0
    };

    this.refreshView = this.refreshView.bind(this);
    Promise.all(this.state.amps.map(amp => amp.doRun())).then(this.refreshView);
  }

  refreshView() {
    this.setState(curState => {
      return curState;
    });
  }

  doRun() {
    if (this.state.currentAmp == 0 && this.state.amps[4].state.memory[this.state.amps[4].state.pointer] == 99) {
      return;
    }
    this.state.amps[this.state.currentAmp].doRun(this.state.signal).then(out => {
      this.setState(curState => {
        curState.cycles += 1;
        curState.currentAmp += 1;
        if (curState.currentAmp > 4) {
          curState.currentAmp -= 5;
        }
        curState.signal = out.output[0];
        return curState;
      });
    });
  }

  render() {
    let ampMemories = this.state.amps.map((amp, index) => {
      return (
        <IntcodeMemory
          key={index}
          memory={amp.state.memory}
          pointer={amp.state.pointer}
          relativeBase={amp.state.relativeBase}
        />
      )
    });
    return (
      <div className="intcode-console">
        {ampMemories}
        <p className="action-details">
          <strong>Cycles:</strong> {this.state.cycles},&nbsp;
          <strong>Current Amp:</strong> {this.state.currentAmp},&nbsp;
          <strong>Signal:</strong> {this.state.signal},&nbsp;
        </p>
        <p className="action-buttons">
          <button onClick={e => this.doRun()}>Run Next Amplifier</button>
        </p>
      </div>
    );
  }
}

const phaseSettings = [
  [0, 1, 2, 3, 4],
  [0, 1, 2, 4, 3],
  [0, 1, 3, 2, 4],
  [0, 1, 3, 4, 2],
  [0, 1, 4, 2, 3],
  [0, 1, 4, 3, 2],

  [1, 0, 2, 3, 4],
  [1, 0, 2, 4, 3],
  [1, 0, 3, 2, 4],
  [1, 0, 3, 4, 2],
  [1, 0, 4, 2, 3],
  [1, 0, 4, 3, 2],

  [0, 2, 1, 3, 4],
  [0, 2, 1, 4, 3],
  [0, 2, 3, 1, 4],
  [0, 2, 3, 4, 1],
  [0, 2, 4, 1, 3],
  [0, 2, 4, 3, 1],

  [2, 0, 1, 3, 4],
  [2, 0, 1, 4, 3],
  [2, 0, 3, 1, 4],
  [2, 0, 3, 4, 1],
  [2, 0, 4, 1, 3],
  [2, 0, 4, 3, 1],

  [0, 3, 1, 2, 4],
  [0, 3, 1, 4, 2],
  [0, 3, 2, 1, 4],
  [0, 3, 2, 4, 1],
  [0, 3, 4, 1, 2],
  [0, 3, 4, 2, 1],

  [3, 0, 1, 2, 4],
  [3, 0, 1, 4, 2],
  [3, 0, 2, 1, 4],
  [3, 0, 2, 4, 1],
  [3, 0, 4, 1, 2],
  [3, 0, 4, 2, 1],

  [0, 4, 1, 2, 3],
  [0, 4, 1, 3, 2],
  [0, 4, 2, 1, 3],
  [0, 4, 2, 3, 1],
  [0, 4, 3, 1, 2],
  [0, 4, 3, 2, 1],

  [4, 0, 1, 2, 3],
  [4, 0, 1, 3, 2],
  [4, 0, 2, 1, 3],
  [4, 0, 2, 3, 1],
  [4, 0, 3, 1, 2],
  [4, 0, 3, 2, 1],

  [1, 2, 0, 3, 4],
  [1, 2, 0, 4, 3],
  [1, 2, 3, 0, 4],
  [1, 2, 3, 4, 0],
  [1, 2, 4, 0, 3],
  [1, 2, 4, 3, 0],

  [2, 1, 0, 3, 4],
  [2, 1, 0, 4, 3],
  [2, 1, 3, 0, 4],
  [2, 1, 3, 4, 0],
  [2, 1, 4, 0, 3],
  [2, 1, 4, 3, 0],

  [1, 3, 0, 2, 4],
  [1, 3, 0, 4, 2],
  [1, 3, 2, 0, 4],
  [1, 3, 2, 4, 0],
  [1, 3, 4, 0, 2],
  [1, 3, 4, 2, 0],

  [3, 1, 0, 2, 4],
  [3, 1, 0, 4, 2],
  [3, 1, 2, 0, 4],
  [3, 1, 2, 4, 0],
  [3, 1, 4, 0, 2],
  [3, 1, 4, 2, 0],

  [1, 4, 0, 2, 3],
  [1, 4, 0, 3, 2],
  [1, 4, 2, 0, 3],
  [1, 4, 2, 3, 0],
  [1, 4, 3, 0, 2],
  [1, 4, 3, 2, 0],

  [4, 1, 0, 2, 3],
  [4, 1, 0, 3, 2],
  [4, 1, 2, 0, 3],
  [4, 1, 2, 3, 0],
  [4, 1, 3, 0, 2],
  [4, 1, 3, 2, 0],

  [2, 3, 0, 1, 4],
  [2, 3, 0, 4, 1],
  [2, 3, 1, 0, 4],
  [2, 3, 1, 4, 0],
  [2, 3, 4, 0, 1],
  [2, 3, 4, 1, 0],

  [3, 2, 0, 1, 4],
  [3, 2, 0, 4, 1],
  [3, 2, 1, 0, 4],
  [3, 2, 1, 4, 0],
  [3, 2, 4, 0, 1],
  [3, 2, 4, 1, 0],

  [2, 4, 0, 1, 3],
  [2, 4, 0, 3, 1],
  [2, 4, 1, 0, 3],
  [2, 4, 1, 3, 0],
  [2, 4, 3, 0, 1],
  [2, 4, 3, 1, 0],

  [4, 2, 0, 1, 3],
  [4, 2, 0, 3, 1],
  [4, 2, 1, 0, 3],
  [4, 2, 1, 3, 0],
  [4, 2, 3, 0, 1],
  [4, 2, 3, 1, 0],

  [3, 4, 0, 1, 2],
  [3, 4, 0, 2, 1],
  [3, 4, 1, 0, 2],
  [3, 4, 1, 2, 0],
  [3, 4, 2, 0, 1],
  [3, 4, 2, 1, 0],

  [4, 3, 0, 1, 2],
  [4, 3, 0, 2, 1],
  [4, 3, 1, 0, 2],
  [4, 3, 1, 2, 0],
  [4, 3, 2, 0, 1],
  [4, 3, 2, 1, 0]
];

function runCyclePhases(program) {
  let promiseList = [];
  phaseSettings.forEach(setting => {
    let cycleSetting = setting.map(s => s + 5);
    let amps = [
      new Amplifier({ phaseSetting: cycleSetting[0], program }),
      new Amplifier({ phaseSetting: cycleSetting[1], program }),
      new Amplifier({ phaseSetting: cycleSetting[2], program }),
      new Amplifier({ phaseSetting: cycleSetting[3], program }),
      new Amplifier({ phaseSetting: cycleSetting[4], program })
    ];

    promiseList.push(new Promise((resolve, reject) => {
      let curAmp = 0;
      let curInput = 0;
      let cycles = 0;

      function doWork() {
        //console.log(cycleSetting, `Amp: ${curAmp}`, `Input ${curInput}`, `Cycles: ${cycles}`);
        amps[curAmp].doRun(curInput).then(out => {
          if (curAmp == 4 && out.memory[out.pointer] == 99) {
            resolve({
              phaseSetting: cycleSetting,
              output: out.output[0]
            });
            return;
          }
          cycles++;
          if (cycles > 100) {
            reject('Stack Overflow!');
            return;
          }
          curAmp += 1;
          if (curAmp > 4) curAmp -= 5;
          if (out.output.length == 0) {
            console.error('No output received!');
            reject({
              error: 'No output received',
              out,
              input: curInput,
              curAmp,
              amp: amps[curAmp].state
            });
            return;
          }
          curInput = out.output[0];
          //console.log(`output received`, out.output);
          doWork();
        });
      }
      doWork();

    }));
  });

  return Promise.all(promiseList).then(rs => {
    return rs.reduce((curMax, output) => {
      if (output.output > curMax.output) {
        return output;
      }
      return curMax
    }, { output: 0 });
  })
}

function runAllPhases(program) {
  let promiseList = [];
  phaseSettings.forEach(setting => {
    let amp1 = new Amplifier({ phaseSetting: setting[0], program });
    let amp2 = new Amplifier({ phaseSetting: setting[1], program });
    let amp3 = new Amplifier({ phaseSetting: setting[2], program });
    let amp4 = new Amplifier({ phaseSetting: setting[3], program });
    let amp5 = new Amplifier({ phaseSetting: setting[4], program });

    promiseList.push(amp1.doRun(0).then(out => {
      return amp2.doRun(out[0]);
    }).then(out => {
      return amp3.doRun(out[0]);
    }).then(out => {
      return amp4.doRun(out[0]);
    }).then(out => {
      return amp5.doRun(out[0]);
    }).then(out => {
      return {
        phaseSetting: setting,
        output: out[0]
      };
    }));
  });
  return Promise.all(promiseList).then(rs => {
    return rs.reduce((curMax, output) => {
      if (output.output > curMax.output) {
        return output;
      }
      return curMax
    }, { output: 0 });
  });
}

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: 'Sample 1', signal: 43210, phaseSetting: [4,3,2,1,0], program: [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0] },
        { label: 'Sample 2', signal: 54321, phaseSetting: [0,1,2,3,4], program: [3,23,3,24,1002,24,10,24,1002,23,-1,23,
            101,5,23,23,1,24,23,23,4,23,99,0,0] },
        { label: 'Sample 3', signal: 65210, phaseSetting: [1,0,4,3,2], program: [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
            1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]}
      ]
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    let testPromises = this.state.testCases.map(test => {
      return runAllPhases(test.program);
    });
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs;
        return curState;
      });
    });
  }

  doWork() {
    performance.mark('work-start');
    const memory = this.props.data[0].split(',').map(i => parseInt(i));
    runAllPhases(memory).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      console.log(rs);
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let unitTests = [];
    if (this.state.testResults !== false) {
      this.state.testCases.forEach((test, index) => {
        unitTests.push({
          label: `Sample ${index + 1} - Signal`,
          expected: test.signal,
          actual: this.state.testResults[index].output
        });
        unitTests.push({
          label: `Sample ${index + 1} - Setting`,
          expected: test.phaseSetting,
          actual: this.state.testResults[index].phaseSetting
        });
      })
    }

    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>Setting: {this.state.puzzleSolution.phaseSetting.join(',' )}<br />Signal: {this.state.puzzleSolution.output}</p>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        <UnitTests tests={unitTests} />
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false,
      testCases: [
        { label: 'Sample 1', signal: 139629729, phaseSetting: [9,8,7,6,5], program: [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5] },
        { label: 'Sample 2', signal: 18216, phaseSetting: [9,7,8,5,6], program: [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10] }
      ]
    };

    this.doWork = this.doWork.bind(this);
    this.runTests = this.runTests.bind(this);
  }

  runTests() {
    performance.mark('tests-start');
    let testPromises = this.state.testCases.map(test => {
      return runCyclePhases(test.program);
    });
    Promise.all(testPromises).then(rs => {
      logPerformanceEnd('tests-start', 'tests-end');
      this.setState(curState => {
        curState.testResults = rs;
        return curState;
      });
    }, rs => {
      logPerformanceEnd('tests-start', 'tests-end');
      console.error(rs.error, rs);
    });
  }

  doWork() {
    const memory = this.props.data[0].split(',').map(i => parseInt(i));
    performance.mark('work-start');
    runCyclePhases(memory).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs;
        return curState;
      });
    });
  }

  render() {
    let unitTests;
    if (this.state.testResults !== false) {
      let tests = [];
      this.state.testCases.forEach((test, index) => {
        tests.push({
          label: test.label + ' - Signal',
          expected: test.signal,
          actual: this.state.testResults[index].output
        });
        tests.push({
          label: test.label + ' - Setting',
          expected: test.phaseSetting,
          actual: this.state.testResults[index].phaseSetting
        });
      });
      unitTests = (
        <UnitTests tests={tests} />
      );
    }

    let solution;
    if (this.state.puzzleSolution !== false) {
      solution = (
        <p>
          <strong>Signal:</strong> {this.state.puzzleSolution.output}<br />
          <strong>Phase Setting:</strong> {this.state.puzzleSolution.phaseSetting.join(', ')}
        </p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p><button onClick={e => this.runTests()}>Run Tests</button></p>
        {unitTests}
        <AmplifierConsole
          phaseSetting={this.state.testCases[0].phaseSetting}
          program={this.state.testCases[0].program}
        />
        <p><button onClick={e => this.doWork()}>Find Solution</button></p>
        {solution}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/07-data.txt');
