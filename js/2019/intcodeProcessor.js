
function getParamsAt({ memory, pointer, paramCount, paramModes, relativeBase }) {
  let rawValues = memory.slice(pointer + 1, pointer + 1 + paramCount);
  let parameters = [];
  for (let i = 0; i < paramCount; i++) {
    const mode = paramModes[i];
    const rawValue = rawValues[i];
    let value;
    let targetAddress = false;
    if (mode === 0) {
      if (typeof memory[rawValue] == 'undefined') {
        value = 0;
      } else {
        value = memory[rawValue];
      }
      targetAddress = rawValue;
    } else if (mode === 1) {
      value = rawValue;
    } else if (mode === 2) {
      targetAddress = rawValue + relativeBase;
      if (typeof memory[targetAddress] == 'undefined') {
        value = 0;
      } else {
        value = memory[targetAddress];
      }
    }
    parameters[i] = {
      mode,
      rawValue,
      value,
      targetAddress
    };
  }
  return parameters
}

function arrayInputFactory(arr) {
  let inputArr = JSON.parse(JSON.stringify(arr));
  return function(destAddress) {
    return inputArr.shift();
  }
}

function promptInputFactory() {
  return function(destAddress) {
    return window.prompt('Enter input value, which will be stored at ' + destAddress);
  }
}

function getInstruction(memory, pointer, relativeBase = 0) {
  let rawInt = memory[pointer];
  let opcode = parseInt(('00' + rawInt).slice(-2));

  // First figure out what the overall instruction is
  let paramCount = 0;
  let label = 'UNKNOWN';
  switch(opcode) {
    case 1:
      label = 'ADD';
      paramCount = 3;
      break;
    case 2:
      label = 'MULTIPLY';
      paramCount = 3;
      break;
    case 3:
      label = 'INPUT';
      paramCount = 1;
      break;
    case 4:
      label = 'OUTPUT';
      paramCount = 1;
      break;
    case 5:
      label = 'JUMP-IF-TRUE';
      paramCount = 2;
      break;
    case 6:
      label = 'JUMP-IF-FALSE';
      paramCount = 2;
      break;
    case 7:
      label = 'LESS-THAN';
      paramCount = 3;
      break;
    case 8:
      label = 'EQUALS';
      paramCount = 3;
      break;
    case 9:
      label = 'RELATIVE-BASE';
      paramCount = 1;
      break;
    case 99:
      label = 'HALT';
      break;
    default:
      label = 'UNKNOWN';
  }

  // Next find out what 'mode' each of the parameters is given in
  let paramModes = [];
  if (rawInt > 99) {
    // Parameter modes given
    let modeFlags = ('' + rawInt).slice(0, -2);
    modeFlags = modeFlags.split('').reverse().join(''); // Reverse the order of the digits
    for (let i = 0; i < paramCount; i++) {
      if (i < modeFlags.length) {
        paramModes[i] = parseInt(modeFlags[i]);
      } else {
        paramModes[i] = 0;
      }
    }
  } else {
    // Parameter modes are all default
    for (let i = 0; i < paramCount; i++) {
      paramModes[i] = 0;
    }
  }

  // Fetch parameter values
  let parameters = getParamsAt({ memory, pointer, paramCount, paramModes, relativeBase });

  return {
    rawInt,
    opcode,
    paramCount,
    parameters,
    label
  }
}

function step({ memory, pointer, input, relativeBase = 0 }) {
  const instruction = getInstruction(memory, pointer, relativeBase);
  let doContinue = true;
  let didJump = false;
  let output = false;
  if (typeof input == 'undefined') {
    input = promptInputFactory();
  }
  switch(instruction.label) {
    case 'ADD': {
      let var1 = instruction.parameters[0].value,
        var2 = instruction.parameters[1].value,
        destAddress = instruction.parameters[2].targetAddress;
      memory[destAddress] = var1 + var2;
      break;
    }
    case 'MULTIPLY': {
      let var1 = instruction.parameters[0].value,
        var2 = instruction.parameters[1].value,
        destAddress = instruction.parameters[2].targetAddress;
      memory[destAddress] = var1 * var2;
      break;
    }
    case 'INPUT': {
      let destAddress = instruction.parameters[0].targetAddress;
      if (typeof input !== 'function') {
        console.error('Input stream is not a function');
        doContinue = false;
      } else {
        let rs = input(destAddress);
        if (rs === null || isNaN(parseInt(rs))) {
          doContinue = false;
        } else {
          memory[destAddress] = parseInt(rs);
        }
      }
      break;
    }
    case 'OUTPUT': {
      output = instruction.parameters[0].value;
      break;
    }
    case 'JUMP-IF-TRUE': {
      if (instruction.parameters[0].value !== 0) {
        pointer = instruction.parameters[1].value;
        didJump = true;
      }
      break;
    }
    case 'JUMP-IF-FALSE': {
      if (instruction.parameters[0].value === 0) {
        pointer = instruction.parameters[1].value;
        didJump = true;
      }
      break;
    }
    case 'LESS-THAN': {
      let destAddress = instruction.parameters[2].targetAddress;
      memory[destAddress] = (instruction.parameters[0].value < instruction.parameters[1].value) ? 1 : 0;
      break;
    }
    case 'EQUALS': {
      let destAddress = instruction.parameters[2].targetAddress;
      memory[destAddress] = (instruction.parameters[0].value === instruction.parameters[1].value) ? 1 : 0;
      break;
    }
    case 'HALT':
      doContinue = false;
      break;
    case 'RELATIVE-BASE': {
      relativeBase += instruction.parameters[0].value;
      break;
    }
    default:
      doContinue = false;
  }
  if (doContinue && !didJump) {
    pointer += instruction.paramCount + 1;
  }
  return {
    pointer: pointer,
    memory: memory,
    lastInstruction: instruction,
    relativeBase: relativeBase,
    doContinue: doContinue,
    output: output
  };
}

function execute({
  memory: initialMemory,
  relativeBase: startingRelativeBase = 0,
  pointer: startingPointer = 0,
  input = [],
  onOutput = false,
  debug=false
}) {
  let memory = JSON.parse(JSON.stringify(initialMemory)); // Don't clobber the input
  let pointer = startingPointer,
    relativeBase = startingRelativeBase,
    lastInstruction,
    doContinue,
    iterations = 0,
    outputLog = [],
    inputHandler;
  if (debug) console.group('Program run');
  if (Array.isArray(input)) {
    inputHandler = arrayInputFactory(input);
  } else if (typeof input == 'function') {
    inputHandler = input;
  } else {
    inputHandler = promptInputFactory();
  }
  return new Promise((resolve, reject) => {
    let workIterations = 0;

    function doStep() {
      let rs = step({ memory, pointer, input: inputHandler, relativeBase });
      ({ pointer, memory, lastInstruction, relativeBase, doContinue } = rs);
      if (debug) console.log(iterations, pointer, lastInstruction, memory);

      let output = rs.output;
      if (output !== false) {
        outputLog.push(output);
        if (typeof onOutput == 'function') {
          let rs = onOutput(output);

          // Check and see if we need to wait for output processing
          if (typeof rs !== 'undefined' && typeof rs.then == 'function') {
            // This is a Promise-like object; wait until it resolves
            return rs;
          }
        }
      }
      return Promise.resolve();
    }

    function workLoop() {
      doStep().then(rs => {
        if (doContinue === false) {
          if (debug) console.groupEnd();
          resolve({
            memory,
            pointer,
            relativeBase,
            output: outputLog
          });
          return;
        }
        workIterations++;
        if (workIterations > 999999) {
          console.error('Recursion Limit Hit!');
          if (debug) console.groupEnd();
          reject({
            error: 'Recursion limit hit',
            memory,
            pointer,
            relativeBase,
            output: outputLog
          });
          return;
        }

        // Otherwise, do another call
        return workLoop();
      }, rs => {
        console.error(rs);
        reject(rs);
      });
    }
    workLoop();

  });
}

export {
  execute,
  step,
  getInstruction
}
