import React from 'react';
import { withData } from 'puzzleUtils';
import ButtonBar from 'ButtonBar';
import UnitTests from 'UnitTests';

function calculateFuel(partMass) {
  return Math.floor(parseInt(partMass) / 3) - 2;
}

function calculateFuelRecursive(partMass) {
  let totalFuel = 0;
  let newFuel = calculateFuel(partMass);
  let iterations = 0;
  console.group(partMass);
  while(iterations < 1000000) {
    if (newFuel <= 0) {
      console.groupEnd();
      return totalFuel;
    }
    console.info(totalFuel, newFuel);
    totalFuel += newFuel;
    newFuel = calculateFuel(newFuel);
    iterations++;
  }
  console.error('Recursion Limit Hit!');
  console.groupEnd();
  return 0;
}

class PartOne extends React.Component {
  render() {
    let total = this.props.data.reduce((subTotal, partMass) => {
      return subTotal + calculateFuel(partMass);
    }, 0);

    let unitTests = [
      { label: '12', expected: 2, actual: calculateFuel(12)},
      { label: '14', expected: 2, actual: calculateFuel(14)},
      { label: '1969', expected: 654, actual: calculateFuel(1969)},
      { label: '100756', expected: 33583, actual: calculateFuel(100756)},
    ];

    return (
      <div>
        <h1>Part One</h1>
        <UnitTests tests={unitTests} />
        <p>Fuel needed: <strong>{total}</strong></p>
      </div>
    );
  }
}


class PartTwo extends React.Component {
  render() {
    let unitTests = [
      { label: '14', expected: 2, actual: calculateFuelRecursive(14)},
      { label: '1969', expected: 966, actual: calculateFuelRecursive(1969)},
      { label: '100756', expected: 50346, actual: calculateFuelRecursive(100756)},
    ];

    let total = this.props.data.reduce((subTotal, partMass) => {
      return subTotal + calculateFuelRecursive(partMass);
    }, 0);

    return (
      <div>
        <h1>Part Two</h1>
        <UnitTests tests={unitTests} />
        <p>Fuel needed: <strong>{total}</strong></p>
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/01-data.txt') ;
