import React from 'react';
import ButtonBar from 'ButtonBar';
import UnitTests from 'UnitTests';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';
import IntcodeConsole from '2019/IntcodeConsole';
import { execute}  from '2019/intcodeProcessor';

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };
  }

  render() {
    let memory = this.props.data[0].split(',').map(value => parseInt(value));

    return (
      <div>
        <h1>Part One</h1>
        <IntcodeConsole memory={memory} />
      </div>
    );
  }
}

class PartTwo extends React.Component {
  render() {

    const posEqualTo = [3,9,8,9,10,9,4,9,99,-1,8];
    const posLessThan = [3,9,7,9,10,9,4,9,99,-1,8];
    const posJumpTo = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9];
    const immediateEqualTo = [3,3,1108,-1,8,3,4,3,99];
    const immediateLessThan = [3,3,1107,-1,8,3,4,3,99];
    const immediateJumpTo = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1];

    let unitTests = [];
    const equalsTests = [
      { input: -10, expected: 0 },
      { input: 0, expected: 0 },
      { input: 7, expected: 0 },
      { input: 8, expected: 1 },
      { input: 9, expected: 0 },
      { input: 50, expected: 0 }
    ];
    const lessThanTests = [
      { input: -10, expected: 1 },
      { input: 0, expected: 1 },
      { input: 7, expected: 1 },
      { input: 8, expected: 0 },
      { input: 9, expected: 0 },
      { input: 50, expected: 0 }
    ];
    const jumpTests = [
      { input: -10, expected: 1 },
      { input: 0, expected: 0 },
      { input: 1, expected: 1 },
      { input: 50, expected: 1 }
    ];

    [
      { program: posEqualTo, mode: 'position', comparator: 'equal to', },
      { program: posLessThan, mode: 'position', comparator: 'less than' },
      { program: immediateEqualTo, mode: 'immediate', comparator: 'equal to' },
      { program: immediateLessThan, mode: 'immediate', comparator: 'less than' },
    ].forEach(program => {
      let tests = (program.comparator == 'equal to') ? equalsTests : lessThanTests;

      tests.forEach(t => {
        let is = (t.expected == 1) ? 'is' : 'is not';
        unitTests.push({
          label: `${t.input} ${is} ${program.comparator} 8 (${program.mode} mode)`,
          expected: t.expected,
          actual: execute({
            memory: program.program,
            input: [t.input]
          }).output[0]
        });
      });
    });

    [
      { program: posJumpTo, mode: 'position' },
      { program: immediateJumpTo, mode: 'immediate' }
    ].forEach(program => {
      jumpTests.forEach(t => {
        unitTests.push({
          label: `${t.input} outputs ${t.expected} (${program.mode} mode JUMP program)`,
          expected: t.expected,
          actual: execute({
            memory: program.program,
            input: [t.input]
          }).output[0]
        });
      });
    });

    const testProg = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
      1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
      999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99];
    [
      { input: -10, expected: 999 },
      { input: 0, expected: 999 },
      { input: 7, expected: 999 },
      { input: 8, expected: 1000 },
      { input: 9, expected: 1001 },
      { input: 50, expected: 1001 }
    ].forEach(t => {
      unitTests.push({
        label: `Larger Comparator: ${t.input} outputs ${t.expected}`,
        expected: t.expected,
        actual: execute({
          memory: testProg,
          input: [t.input]
        }).output[0]
      });
    });

    return (
      <div>
        <h1>Part Two</h1>
        <UnitTests tests={unitTests} />
        <IntcodeConsole memory={[3,9,8,9,10,9,4,9,99,-1,8]} />
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/05-data.txt');
