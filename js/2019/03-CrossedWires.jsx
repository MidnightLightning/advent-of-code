import React from 'react';
import { withData } from 'puzzleUtils';
import ButtonBar from 'ButtonBar';
import UnitTests from 'UnitTests';
import { WorkerQueue } from 'workerUtils';
import * as d3 from 'd3';

function pointId([x, y]) {
  return ('000000' + x).slice(-6) + ':' + ('000000' + y).slice(-6);
}

function getDeltas(direction) {
  let xDelta = 0, yDelta = 0;
  switch (direction) {
    case 'U':
      yDelta =  -1;
      break;
    case 'D':
      yDelta = 1;
      break;
    case 'L':
      xDelta = -1;
      break;
    case 'R':
      xDelta = 1;
      break;
  }
  return [xDelta, yDelta];
}

function crossedWirePoint(wires, debug = false) {
  let passedPoints = {};
  let position = [0, 0];
  if (debug) console.groupCollapsed('Wire set');
  wires[0].forEach(instruction => {
    const direction = instruction[0];
    const distance = parseInt(instruction.slice(1));
    let [xDelta, yDelta] = getDeltas(direction);
    for (let i = 0; i < distance; i++) {
      position[0] += xDelta;
      position[1] += yDelta;
      if (debug) console.log(instruction, i, pointId(position), 'passed by wire 1');
      passedPoints[pointId(position)] = true;
    }
  });

  let crosses = [];
  position = [0, 0];
  wires[1].forEach(instruction => {
    const direction = instruction[0];
    const distance = parseInt(instruction.slice(1));
    let [xDelta, yDelta] = getDeltas(direction);
    for (let i = 0; i < distance; i++) {
      position[0] += xDelta;
      position[1] += yDelta;
      let id = pointId(position);
      if (passedPoints[id] === true) {
        if (debug) console.log(instruction, i, id, 'crossed point');
        crosses.push([position[0], position[1]]);
      } else {
        if (debug) console.log(instruction, i, id, 'not a cross');
      }
    }
  });
  if (debug) console.groupEnd();
  return crosses;
}

class CrossedWiresViz extends React.Component {
  constructor(props) {
    super(props);

    this._vizRef = React.createRef();
    this.paint = this.paint.bind(this);
  }

  componentDidMount() {
    this.paint();
  }

  paint() {
    const data = this.props.data;
    const MARGIN_SIZE = 10;
    const MAX_WIDTH = 1500;
    let svg = d3.select(this._vizRef.current);
    let wireColor = d3.scaleOrdinal([0, 1], ['red', 'blue']);

    let max = [0, 0];
    let min = [0, 0];
    data.forEach(wire => {
      let position = [0,0];
      wire.forEach(instruction => {
        const direction = instruction[0];
        const distance = parseInt(instruction.slice(1));
        let [xDelta, yDelta] = getDeltas(direction);
        position[0] += xDelta * distance;
        position[1] += yDelta * distance;
        if (position[0] < min[0]) min[0] = position[0];
        if (position[0] > max[0]) max[0] = position[0];
        if (position[1] < min[1]) min[1] = position[1];
        if (position[1] > max[1]) max[1] = position[1];
      });
    });

    let overallWidth = max[0] - min[0];
    let overallHeight = max[1] - min[1];
    let widthFactor = MAX_WIDTH / overallWidth;
    if (widthFactor > 1) widthFactor = 1;
    overallWidth += (MARGIN_SIZE / widthFactor) * 2;
    overallHeight += (MARGIN_SIZE / widthFactor) * 2;
    let lineStrokeWidth = 2 / widthFactor;
    let connectionSize = 3 / widthFactor;
    svg
      .attr('viewBox', `${min[0] - MARGIN_SIZE / widthFactor} ${min[1] - MARGIN_SIZE / widthFactor} ${overallWidth} ${overallHeight}`)
      .attr('style', `width: ${overallWidth * widthFactor}px; height: ${overallHeight * widthFactor}px; background: rgba(255,255,255, 0.5); display: block; margin: 1rem 0;`);

    svg.select('#origin').attr('r', connectionSize);
    let crossPoints = crossedWirePoint(data);
    let crosses = svg.select('#connections').selectAll('circle')
      .data(crossPoints);

    crosses.enter().append('circle')
      .attr('cx', d => d[0])
      .attr('cy', d => d[1])
      .attr('r', connectionSize)
      .attr('style', 'stroke: transparent; fill: rgba(255, 255, 0, 0.7);');

    let wires = svg.select('#wires').selectAll('path')
      .data(data);

    wires.enter().append('path')
      .attr('style', (d, i) => {
        return `fill: transparent; stroke: ${wireColor(i)}; stroke-width:${lineStrokeWidth}px; stroke-linejoin:round; stroke-linecap:round`;
      })
      .attr('d', d => {
        let path = d3.path();
        path.moveTo(0,0);
        let position = [0, 0];
        d.forEach(instruction => {
          const direction = instruction[0];
          const distance = parseInt(instruction.slice(1));
          let [xDelta, yDelta] = getDeltas(direction);
          position[0] += xDelta * distance;
          position[1] += yDelta * distance;
          path.lineTo(position[0], position[1]);
        });

        return path.toString();
      });
  }

  render() {
    return (
      <svg ref={this._vizRef}>
        <g id="wires" />
        <g id="connections">
          <circle id="origin" cx="0" cy="0" r="8" style={{ stroke: 'transparent', fill: 'green' }} />
        </g>
      </svg>
    );
  }
}

let testOne = ['R8,U5,L5,D3'.split(','), 'U7,R6,D4,L4'.split(',')];
let testTwo = ['R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','), 'U62,R66,U55,R34,D71,R55,D58,R83'.split(',')];
let testThree = ['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','), 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(',')];

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this._viz = React.createRef();
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/03-worker.js'));

    let testPromises = [
      this._worker.send({ cmd: 'crossed-wire-distance', wires: testOne }),
      this._worker.send({ cmd: 'crossed-wire-distance', wires: testTwo }),
      this._worker.send({ cmd: 'crossed-wire-distance', wires: testThree }),
    ];
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });

    const wires = this.props.data.map(row => row.split(','));
    this._worker.send({ cmd: 'crossed-wire-distance', wires: wires}).then(rs => {
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    const wires = this.props.data.map(row => row.split(','));

    let testResults;
    if (this.state.testResults !== false) {
      let unitTests = [
        { label: 'Test One', expected: 6, actual: this.state.testResults[0]},
        { label: 'Test Two', expected: 159, actual: this.state.testResults[1]},
        { label: 'Test Three', expected: 135, actual: this.state.testResults[2]},
      ];
      testResults = (
        <UnitTests tests={unitTests} />
      );
    }

    let output = this.state.puzzleSolution;

    return (
      <div>
        <h1>Part One</h1>
        {testResults}
        <CrossedWiresViz data={testTwo} />
        <CrossedWiresViz data={wires} ref={this._viz} />
        <p><button onClick={e => {
          let svgImage = this._viz.current._vizRef.current;
          let svgXML = (new XMLSerializer).serializeToString(svgImage);
          let element = document.createElement('a');
          element.setAttribute('href', `data:image/svg+xml;charset=utf-8,${svgXML}`);
          element.setAttribute('download', 'Viz.svg');
          document.body.appendChild(element);
          element.click();
          document.body.removeChild(element);
        }}>Download</button></p>
        <p>{output}</p>
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      result: false
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/03-worker.js'));

    let testPromises = [
      this._worker.send({ cmd: 'crossed-wire-signal', wires: testOne }),
      this._worker.send({ cmd: 'crossed-wire-signal', wires: testTwo }),
      this._worker.send({ cmd: 'crossed-wire-signal', wires: testThree }),
    ];
    Promise.all(testPromises).then(rs => {
      this.setState(curState => {
        curState.testResults = rs.map(row => row.result);
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doWork() {
    const wires = this.props.data.map(row => row.split(','));
    this._worker.send({
      cmd: 'crossed-wire-signal',
      wires: wires
    }).then(rs => {
      this.setState(curState => {
        curState.result = rs.result;
        return curState;
      });
    });
  }

  render() {
    let testResults;
    if (this.state.testResults !== false) {
      let unitTests = [
        { label: 'Test One', expected: 30, actual: this.state.testResults[0]},
        { label: 'Test Two', expected: 610, actual: this.state.testResults[1]},
        { label: 'Test Three', expected: 410, actual: this.state.testResults[2]},
      ];
      testResults = (
        <UnitTests tests={unitTests} />
      );
    }

    let output;
    if (this.state.result !== false) {
      output = (
        <p>{this.state.result}</p>
      );
    }

    return (
      <div>
        <h1>Part Two</h1>
        {testResults}
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/03-data.txt');
