import React from 'react';
import ButtonBar from 'ButtonBar';
import { WorkerQueue } from 'workerUtils';
import { showAsyncTestResults, logPerformanceEnd, withData } from 'puzzleUtils';

class PartOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/08-worker.js'));

    const data = this.props.data[0];
    this._worker.send({
      cmd: 'solve-puzzle',
      data: data,
      width: 25,
      height: 6
    }).then(rs => {
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      output = (
        <p>{this.state.puzzleSolution}</p>
      );
    }

    return (
      <div>
        <h1>Part One</h1>
        {output}
      </div>
    );
  }
}

class PartTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testResults: false,
      puzzleSolution: false
    };

    this.doWork = this.doWork.bind(this);
  }

  componentDidMount() {
    this._worker = new WorkerQueue(new Worker('js/workers/08-worker.js'));
  }

  componentWillUnmount() {
    this._worker.terminate();
  }

  doWork() {
    performance.mark('work-start');
    const data = this.props.data[0];
    this._worker.send({
      cmd: 'render-image',
      data: data,
      width: 25,
      height: 6
    }).then(rs => {
      logPerformanceEnd('work-start', 'work-end');
      this.setState(curState => {
        curState.puzzleSolution = rs.result;
        return curState;
      });
    });
  }

  render() {
    let output;
    if (this.state.puzzleSolution !== false) {
      output = this.state.puzzleSolution.map(row => {
        let blocks = [];
        for (let i = 0; i < row.length; i++) {
          let style = { display: 'inline-block', width: 10, height: 10 };
          if (row[i] == 0) {
            style.background = 'black';
          } else {
            style.background = 'white';
          }
          blocks.push(<span style={style} />);
        }
        return (
          <p style={{ margin: 0, padding: 0 }}>{blocks}</p>
        );
      });
    }

    return (
      <div>
        <h1>Part Two</h1>
        <p>
          <button onClick={e => this.doWork()}>Run</button>
        </p>
        {output}
      </div>
    );
  }
}


class PuzzleView extends React.Component {
  render() {
    let BodyContent = (this.props.navPath[1] == 'part-2') ? PartTwo : PartOne;
    return (
      <section>
        <ButtonBar base={this.props.navPath[0]} />
        <BodyContent data={this.props.data} />
      </section>
    );
  }
}

export default withData(PuzzleView, '/data/2019/08-data.txt');
