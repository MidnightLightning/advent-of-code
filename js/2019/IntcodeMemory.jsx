import React from 'react';
import { getInstruction } from '2019/intcodeProcessor';

function chunkArray(arr, chunkLength) {
  let tmp = [];
  for (let i = 0; i < arr.length; i+=chunkLength) {
    tmp.push(arr.slice(i, i + chunkLength));
  }
  return tmp;
}

class IntcodeMemory extends React.Component {
  render() {
    const ROW_LENGTH = 20;
    const { memory, pointer, relativeBase } = this.props;

    let headerCells = [
      <th key="corner">&nbsp;</th>
    ];
    for (let i = 0; i < ROW_LENGTH; i++) {
      let index = ('00'+i).slice(-2);
      let className = 'col-' + index;
      headerCells.push(<th key={index} className={className}>{index}</th>)
    }

    let currentInstruction = getInstruction(memory, pointer, relativeBase);

    let activeMemoryCells = {};
    currentInstruction.parameters.forEach(param => {
      if (param.targetAddress !== false) {
        activeMemoryCells[param.targetAddress] = true;
      }
    });

    let memoryRows = chunkArray(memory, ROW_LENGTH).map((row, rowNum) => {
      let startAddress = ('000000' + (rowNum * ROW_LENGTH)).slice(-6);
      let rowCells = [
        <th key="row-header">{startAddress}</th>
      ];
      for (let cellNum = 0; cellNum < ROW_LENGTH; cellNum++) {
        const cell = typeof (row[cellNum]) == 'undefined' ? 0 : row[cellNum];
        let addressNum = rowNum * ROW_LENGTH + cellNum;
        let address = ('000000' + addressNum).slice(-6);
        let colNum = ('00' + cellNum).slice(-2);
        let className = `col-${colNum} address-${address}`;
        if (addressNum == pointer) {
          className += ' current-pointer';
        }
        if (activeMemoryCells[addressNum] === true) {
          className += ' highlighted';
        }
        if (addressNum - pointer > 0 && addressNum - pointer <= currentInstruction.paramCount) {
          className += ' current-parameter';
        }
        rowCells.push(
          <td key={cellNum} className={className} title={address}>{cell}</td>
        );
      }
      return rowCells;
    });

    return (
      <table className="intcode-memory" cellPadding="0" cellSpacing="0">
        <thead>
        <tr>{headerCells}</tr>
        </thead>
        <tbody>
        {
          memoryRows.map((row, index) => {
            return (
              <tr key={index}>{row}</tr>
            );
          })
        }
        </tbody>
      </table>
    );
  }
}

export default IntcodeMemory;
