import React from 'react';
import dispatcher from "./dispatcher";
import navData from 'navData';

function NavList({ routes, currentPath }) {
  return (
    <ul>
      {
        routes.map(page => {
          let className = '';
          if (page.id == currentPath) {
            className = 'active';
          }
          return (
            <li
              onClick={e => dispatcher.publish('navigateTo', page.id)}
              id={page.id}
              key={page.id}
              className={className}
            >{page.label}</li>
          );
        })
      }
    </ul>
  );
}

class NavView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: {
        'Main': true,
        '2020': true
      }
    };

    this.toggleSection = this.toggleSection.bind(this);
  }

  toggleSection(label) {
    this.setState(curState => {
      if (typeof curState.expanded[label] == 'undefined') {
        curState.expanded[label] = true;
      } else {
        delete curState.expanded[label];
      }
      return curState;
    });
  }

  render() {
    return (
      <nav>
        {
          navData.map(section => {
            if (this.state.expanded[section.label] !== true) {
              // Show collapsed
              return (
                <h2
                  key={section.label}
                  style={{ cursor: 'pointer' }}
                  onClick={e => this.toggleSection(section.label)}
                >{section.label}</h2>
              );
            }

            // Show expanded
            return (
              <React.Fragment key={section.label}>
                <h2
                  style={{ cursor: 'pointer' }}
                  onClick={e => this.toggleSection(section.label)}
                >{section.label}</h2>
                <NavList routes={section.routes} currentPath={this.props.navPath[0]} />
              </React.Fragment>
            );
          })
        }
      </nav>
    );
  }
}

export default NavView;
